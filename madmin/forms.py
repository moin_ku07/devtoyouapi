from django.forms import ModelForm, PasswordInput, CharField
from api.models import Store, Customer, StoreBranch


class StoreForm(ModelForm):
    class Meta:
        model = Store
        fields = ('name', 'thumb', 'description', 'avg_time_spent', 'store_type', 'favorite', 'delivery_charge')


class StoreBranchForm(ModelForm):
    class Meta:
        model = StoreBranch
        fields = ('branch_name', 'district', 'phone', 'longitude', 'latitude')

class CustomerForm(ModelForm):
    password = CharField(widget=PasswordInput, required=False)
    email = CharField(required=False)
    class Meta:

        model = Customer
        fields = (
            'username', 'password', 'first_name', 'last_name', 'email', 'gender',
            'loyalty_amount', 'deposit_amount')
