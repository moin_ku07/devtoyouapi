# -*- coding: utf-8 -*-
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView
from django.shortcuts import render, redirect
from django.http.response import HttpResponse
from api.models import Customer, Driver, UserLocation, Store, Bank, Order, StoreItem, StoreBranch, StoreOwner, \
    DriverEvaluation, BankAccount, DriverGPS, StoreType, PaymentMethod, HiddenDriverEvaluation, AppSetting, \
    UserBalance, User, StorePhoto, District, OrderStatusChange
from django.db.models import Sum, Q
from push_notifications.models import APNSDevice, GCMDevice
import json
from django.contrib.auth.decorators import login_required
from django import forms
from django.contrib import messages
from .forms import StoreForm, CustomerForm, StoreBranchForm

PAGINATE_BY = 20


def madmin_home(request):
    return redirect("/madmin/order/list")


class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)


class DriverListView(LoginRequiredMixin, ListView):
    model = Driver
    paginate_by = PAGINATE_BY

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data()
        context['query'] = self.request.GET.get('query', None)
        return context

    def get_queryset(self):
        request = self.request
        sort = request.GET.get('sort', None)
        query = request.GET.get('query', 'None')

        if sort:
            if sort == request.session.get('driver_sort', 'id'):
                if request.session.get('sort_order', '-'):
                    request.session['sort_order'] = ""
                else:
                    request.session['sort_order'] = "-"

            request.session['driver_sort'] = sort

        drivers = Driver.objects.all().order_by(
            "{}{}".format(request.session.get('sort_order', '-'), request.session.get('driver_sort', 'id')))

        if query != 'None':
            drivers = drivers.filter(Q(username__icontains=query) |
                                     Q(first_name__icontains=query) |
                                     Q(last_name__icontains=query) |
                                     Q(notes__icontains=query))

        return drivers


class DriverCreateView(LoginRequiredMixin, CreateView):
    model = Driver
    success_url = '/madmin/driver/list'
    fields = ('username', 'password', 'email', 'first_name', 'last_name', 'notes', 'is_active')

    def get_form(self, klass=None):
        form = super(DriverCreateView, self).get_form(klass)
        form.fields['password'].widget = forms.PasswordInput()
        return form

    def form_valid(self, form):
        super_response = super(DriverCreateView, self).form_valid(form)

        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = Driver.objects.get(username=username)
        user.set_password(password)
        user.save()
        return super_response


class DriverUpdateView(LoginRequiredMixin, UpdateView):
    model = Driver
    success_url = '/madmin/driver/list'
    fields = ('username', 'password', 'email', 'first_name', 'last_name', 'notes', 'is_active')

    def get_form(self, klass=None):
        form = super(DriverUpdateView, self).get_form(klass)
        form.fields['password'].widget = forms.PasswordInput()
        form.fields['password'].required = False
        return form

    def form_valid(self, form):
        super_response = super(DriverUpdateView, self).form_valid(form)

        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = Driver.objects.get(username=username)
        if password != "":
            user.set_password(password)
            user.save()
        return super_response


class AllOrderListView(LoginRequiredMixin, ListView):
    model = Order
    paginate_by = PAGINATE_BY

    STATUS_CHOICES = (
        ('REQ', u'طلب جديد'),
        ('REV', u'مراجعة'),
        ('APPR', u'معتمد'),
        ('AD', u'مسند إلى المندوب'),
        ('DOTW', u'المندوب في الطريق'),
        ('DELIV', u'تم التوصيل'),
        ('MCL', u'مغلق من المشرف'),
        ('UCAN', u'ملغي من العميل'),
    )

    def get_context_data(self, **kwargs):
        context = super(AllOrderListView, self).get_context_data()
        context['is_pending'] = False
        context['drivers'] = Driver.objects.all()
        context['driver'] = self.request.GET.get('driver', None)
        context['status'] = self.request.GET.get('status', None)
        context['status_choices'] = self.STATUS_CHOICES
        context['query'] = self.request.GET.get('query', None)

        return context

    def get_queryset(self):
        request = self.request
        driver_id = request.GET.get('driver', 'None')
        status = request.GET.get('status', 'All')
        sort = request.GET.get('sort', None)
        query = request.GET.get('query', 'None')

        if sort:
            if sort == request.session.get('order_sort', 'id'):
                if request.session.get('sort_order', '-'):
                    request.session['sort_order'] = ""
                else:
                    request.session['sort_order'] = "-"

            request.session['order_sort'] = sort

        orders = Order.objects.all()
        if driver_id != 'None':
            orders = orders.filter(driver__id=driver_id)
        if status != 'All' and status != 'None':
            orders = orders.filter(status=status)

        orders = orders.order_by(
            "{}{}".format(request.session.get('sort_order', '-'), request.session.get('order_sort', 'id')))

        if query != 'None':
            orders = orders.filter(
                Q(additional_notes__icontains=query) |
                Q(user__username__icontains=query) |
                Q(driver__username__icontains=query)
            )

        return orders


class PendingOrderListView(LoginRequiredMixin, ListView):
    model = Order
    paginate_by = PAGINATE_BY

    STATUS_CHOICES = (
        ('REQ', u'طلب جديد'),
        ('REV', u'مراجعة'),
        ('APPR', u'معتمد'),
        ('AD', u'مسند إلى المندوب'),
        ('DOTW', u'المندوب في الطريق'),
        ('DELIV', u'تم التوصيل'),
        ('MCL', u'مغلق من المشرف'),
        ('UCAN', u'ملغي من العميل'),
        ('MREJ', u'طلب مرفوض')

    )

    def get_context_data(self, **kwargs):
        context = super(PendingOrderListView, self).get_context_data()
        context['is_pending'] = True
        context['drivers'] = Driver.objects.all()
        context['status_choices'] = self.STATUS_CHOICES
        return context

    def get_queryset(self):
        pending_status = ['REQ', 'REV']
        return Order.objects.all().filter(status__in=pending_status)


class DriverEvaluationListView(LoginRequiredMixin, ListView):
    model = DriverEvaluation
    paginate_by = PAGINATE_BY

    def get_context_data(self, **kwargs):
        context = super(DriverEvaluationListView, self).get_context_data()
        context['driver'] = self.request.GET.get('driver', None)

        return context

    def get_queryset(self):
        request = self.request
        driver_id = request.GET.get('driver', 'None')
        sort = request.GET.get('sort', None)

        if sort:
            if sort == request.session.get('de_sort', 'id'):
                if request.session.get('sort_order', '-'):
                    request.session['sort_order'] = ""
                else:
                    request.session['sort_order'] = "-"

            request.session['de_sort'] = sort

        de = DriverEvaluation.objects.all()
        if driver_id != 'None':
            de = de.filter(driver__id=driver_id)
        de = de.order_by("{}{}".format(request.session.get('sort_order', '-'), request.session.get('de_sort', 'id')))

        return de


class OrderCreateView(LoginRequiredMixin, CreateView):
    model = Order
    success_url = '/madmin/order/list'
    fields = ('user', 'driver', 'status', 'pickup', 'additional_notes', 'total',)


class OrderUpdateView(LoginRequiredMixin, UpdateView):
    model = Order
    success_url = '/madmin/order/list'
    fields = ('user', 'driver', 'status', 'pickup', 'additional_notes', 'total',)


class OrderDetailView(LoginRequiredMixin, DetailView):
    model = Order

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data()
        order = self.get_object()
        context['status_changes'] = OrderStatusChange.objects.filter(order=order)

        return context


@login_required
def track_order(request):
    orders = request.GET.get('orders', '')
    if orders != '':
        orders = orders.split(',')
    else:
        orders = []

    return render(request, 'madmin/track_order.html', {"orders": json.dumps(orders)})


@login_required
def track_driver(request):
    driver = request.GET.get('driver', None)
    if driver:
        driver_gps = DriverGPS.objects.all().filter(driver__id=driver).order_by('-id')
        if driver_gps:
            driver = driver_gps[0]
            return render(request, 'madmin/track_driver.html', {'driver': driver})
        else:
            return HttpResponse("The driver does not yet have any locations!")
    else:
        return HttpResponse("Driver not found!")


@login_required
def assign_driver(request):
    orders = request.GET.get('orders', '')
    driver_id = request.GET.get('driver', None)
    if orders != '':
        orders = orders.split(',')
    else:
        orders = []

    if driver_id:
        driver = Driver.objects.get(pk=driver_id)
        for order_id in orders:
            try:
                order = Order.objects.get(pk=order_id)
                order.status = 'AD'
                order.driver = driver
                order.save()
            except Exception as ex:
                print ex

        return render(request, 'madmin/assign_driver.html', {'driver': driver})

    else:
        return HttpResponse("No driver found with the ID!")


@login_required
def update_order_status(request):
    orders = request.GET.get('orders', '')
    status = request.GET.get('status', None)
    if orders != '':
        orders = orders.split(',')
    else:
        orders = []

    if status:
        for order_id in orders:
            try:
                order = Order.objects.get(pk=order_id)
                order.status = status
                order.save()
            except Exception as ex:
                print ex

        return render(request, 'madmin/update_order_status.html', {'status': status})

    else:
        return HttpResponse("No driver found with the ID!")


class CustomerListView(LoginRequiredMixin, ListView):
    model = Customer
    paginate_by = PAGINATE_BY

    def get_context_data(self, **kwargs):
        context = super(CustomerListView, self).get_context_data()
        context['query'] = self.request.GET.get('query', None)

        return context

    def get_queryset(self):
        request = self.request
        sort = request.GET.get('sort', None)
        query = request.GET.get('query', 'None')

        if sort:
            if sort == request.session.get('customer_sort', 'id'):
                if request.session.get('sort_order', '-'):
                    request.session['sort_order'] = ""
                else:
                    request.session['sort_order'] = "-"

            request.session['customer_sort'] = sort

        customers = Customer.objects.all().order_by(
            "{}{}".format(request.session.get('sort_order', '-'), request.session.get('customer_sort', 'id')))

        if query != 'None':
            customers = customers.filter(Q(username__icontains=query) |
                                         Q(first_name__icontains=query) |
                                         Q(last_name__icontains=query))

        return customers


class CustomerCreateView(LoginRequiredMixin, CreateView):
    initial = {'deposit_amount': 0.00, 'loyalty_amount': 0.00}
    model = Customer
    success_url = '/madmin/customer/list'
    fields = ('username', 'password', 'email', 'gender', 'loyalty_amount', 'deposit_amount')

    def get_form(self, klass=None):
        form = super(CustomerCreateView, self).get_form(klass)
        form.fields['password'].widget = forms.PasswordInput()
        return form

    def form_valid(self, form):
        super_response = super(CustomerCreateView, self).form_valid(form)

        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = self.model.objects.get(username=username)
        user.set_password(password)
        user.save()
        return super_response


class CustomerUpdateView(LoginRequiredMixin, UpdateView):
    model = Customer
    form_class = CustomerForm
    success_url = '/madmin/customer/list'

    def form_valid(self, form):
        super_response = super(CustomerUpdateView, self).form_valid(form)

        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = self.model.objects.get(username=username)
        if not password.startswith("pbkdf2_sha256$") and password != '':
            user.set_password(password)
            user.save()
        return super_response


class UserLocationListView(LoginRequiredMixin, ListView):
    model = UserLocation


class UserLocationCreateView(LoginRequiredMixin, CreateView):
    model = UserLocation
    success_url = '/madmin/user-location/list'
    fields = ('user', 'name', 'district', 'phone', 'street', 'house_no', 'door_no',
              'door_color', 'note', 'longitude', 'latitude')


class UserLocationUpdateView(LoginRequiredMixin, UpdateView):
    model = UserLocation
    success_url = '/madmin/user-location/list'
    fields = ('user', 'name', 'district', 'phone', 'street', 'house_no', 'door_no',
              'door_color', 'note', 'longitude', 'latitude')


class StoreListView(LoginRequiredMixin, ListView):
    model = Store
    paginate_by = PAGINATE_BY

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data()
        context['query'] = self.request.GET.get('query', None)
        return context

    def get_queryset(self):
        request = self.request
        sort = request.GET.get('sort', None)
        query = request.GET.get('query', 'None')

        if sort:
            if sort == request.session.get('store_sort', 'id'):
                if request.session.get('sort_order', '-'):
                    request.session['sort_order'] = ""
                else:
                    request.session['sort_order'] = "-"

            self.request.session['store_sort'] = sort

        stores = Store.objects.all().order_by(
            "{}{}".format(request.session.get('sort_order', '-'), request.session.get('store_sort', 'id')))

        if query != 'None':
            stores = stores.filter(Q(name__icontains=query) |
                                   Q(description__icontains=query))

        return stores


class StoreCreateView(LoginRequiredMixin, CreateView):
    model = Store
    success_url = '/madmin/store/list'
    fields = ('name', 'thumb', 'description', 'avg_time_spent', 'store_type', 'favorite')

    def get_success_url(self):
        return "/madmin/store-owner/create?store={}".format(self.object.id)


def store_create(request):
    if request.method == "POST":

        store_form = StoreForm(request.POST, request.FILES)
        store_branch_form = StoreBranchForm(request.POST, request.FILES)

        if store_form.is_valid():
            store = store_form.save()
            if store_branch_form.is_valid():
                store_branch = store_branch_form.save(commit=False)
                store_branch.store = store
                store_branch.save()
            return redirect("/madmin/store/list")

        else:
            return render(request, 'madmin/store_create.html',
                          {"store_form": store_form, "branch_form": store_branch_form})


    else:
        store_form = StoreForm()
        store_branch_form = StoreBranchForm()
        return render(request, 'madmin/store_create.html',
                      {"store_form": store_form, "branch_form": store_branch_form})


def store_owner_form(request):
    store_id = request.GET.get('store')

    if store_id:
        store_instance = Store.objects.get(pk=store_id)
    else:
        store_instance = None

    owners = StoreOwner.objects.all().filter(store=store_instance)
    all_customers = Customer.objects.all()

    if len(owners) > 0:
        owner = owners[0].customer
    else:
        owner = None

    if request.method == "POST":
        store_branch = None
        store_form = StoreForm(request.POST, request.FILES, instance=store_instance)
        customer_form = CustomerForm(request.POST, request.FILES, instance=owner)

        if store_form.is_valid():
            store = store_form.save(commit=False)
            if store_instance is None:
                branch_form = StoreBranchForm(request.POST, request.FILES)
                if branch_form.is_valid():
                    store_branch = branch_form.save(commit=False)
                else:
                    store_branch = None

        else:
            store = None
            store_branch = None

        customer_id = request.POST.get("customer_id", "none")
        if customer_id != "none":
            user = Customer.objects.get(pk=int(customer_id))
        else:
            if customer_form.is_valid():
                user = customer_form.save(commit=False)
                password = user.password
                if not password.startswith("pbkdf2_sha256$"):
                    user.set_password(password)
            else:
                user = None

        if user and store:
            user.save()
            store.save()

            if store_branch:
                store_branch.store = store
                store_branch.save()

            store_owner, created = StoreOwner.objects.get_or_create(customer=user, store=store)
            store_owner.save()



        else:
            data = {
                "store_form": store_form,
                "all_customers": all_customers
            }
            customer_id = request.POST.get("customer_id", "none")
            if customer_id == "none":
                data['customer_form'] = customer_form

            if store_instance is None:
                data['branch_form'] = StoreBranchForm(request.POST, request.FILES)

            return render(request, 'madmin/store_create.html', data)

        return redirect("/madmin/store/ownerform?store={}&msg=The details updated!".format(store.id))

    else:
        store_form = StoreForm(instance=store_instance)
        customer_form = CustomerForm(instance=owner)
        msg = request.GET.get('msg', None)
        data = {
            "store_form": store_form,
            "customer_form": customer_form,
            "all_customers": all_customers
        }

        if msg:
            data['msg'] = msg

        if store_instance is None:
            data['branch_form'] = StoreBranchForm()

        return render(request, 'madmin/store_create.html', data)


def store_details(request):
    store_id = request.GET.get('store')

    if store_id:
        store_instance = Store.objects.get(pk=store_id)
    else:
        store_instance = None

    owners = StoreOwner.objects.all().filter(store=store_instance)

    if len(owners) > 0:
        owner = owners[0].customer
    else:
        owner = None

    return render(request, 'madmin/store_details.html',
                  {"store": store_instance, "customer": owner})


class StoreUpdateView(LoginRequiredMixin, UpdateView):
    model = Store
    success_url = '/madmin/store/list'
    fields = ('name', 'thumb', 'description', 'avg_time_spent', 'store_type', 'favorite')


class StoreItemListView(LoginRequiredMixin, ListView):
    model = StoreItem
    paginate_by = PAGINATE_BY

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data()
        context['query'] = self.request.GET.get('query', None)

        return context

    def get_queryset(self):
        request = self.request
        sort = request.GET.get('sort', None)
        query = request.GET.get('query', 'None')

        if sort:
            if sort == request.session.get('si_sort', 'id'):
                if request.session.get('sort_order', '-'):
                    request.session['sort_order'] = ""
                else:
                    request.session['sort_order'] = "-"

            request.session['si_sort'] = sort

        items = StoreItem.objects.all().order_by(
            "{}{}".format(request.session.get('sort_order', '-'), request.session.get('si_sort', 'id')))

        if query != 'None':
            items = items.filter(Q(name__icontains=query) |
                                 Q(description__icontains=query))

        return items


class StoreItemCreateView(LoginRequiredMixin, CreateView):
    model = StoreItem
    success_url = '/madmin/store-item/list'
    fields = ('store', 'name', 'description', 'photo', 'price', 'favorite')


class StoreItemUpdateView(LoginRequiredMixin, UpdateView):
    model = StoreItem
    success_url = '/madmin/store-item/list'
    fields = ('store', 'name', 'description', 'photo', 'price', 'favorite')


class StoreBranchListView(LoginRequiredMixin, ListView):
    model = StoreBranch
    paginate_by = PAGINATE_BY

    def get_queryset(self):
        request = self.request
        sort = request.GET.get('sort', None)

        if sort:
            if sort == request.session.get('sb_sort', 'id'):
                if request.session.get('sort_order', '-'):
                    request.session['sort_order'] = ""
                else:
                    request.session['sort_order'] = "-"

            request.session['sb_sort'] = sort

        return StoreBranch.objects.all().order_by(
            "{}{}".format(request.session.get('sort_order', '-'), request.session.get('sb_sort', 'id')))


class StoreBranchCreateView(LoginRequiredMixin, CreateView):
    model = StoreBranch
    success_url = '/madmin/store-branch/list'
    fields = ('store', 'branch_name', 'district', 'phone', 'longitude', 'latitude')


class StoreBranchUpdateView(LoginRequiredMixin, UpdateView):
    model = StoreBranch
    success_url = '/madmin/store-branch/list'
    fields = ('store', 'branch_name', 'district', 'phone', 'longitude', 'latitude')


class BankListView(LoginRequiredMixin, ListView):
    model = Bank
    paginate_by = PAGINATE_BY


class BankCreateView(LoginRequiredMixin, CreateView):
    model = Bank
    success_url = '/madmin/bank/list'
    fields = ('name',)


class BankUpdateView(LoginRequiredMixin, UpdateView):
    model = Bank
    success_url = '/madmin/bank/list'
    fields = ('name',)


class DistrictCreatView(LoginRequiredMixin, CreateView):
    model = District
    success_url = '/madmin/district/list'
    fields = ('name',)


class DistrictUpdateView(LoginRequiredMixin, UpdateView):
    model = District
    success_url = '/madmin/district/list'
    fields = ('name',)


class DistrictListView(ListView):
    model = District
    paginate_by = PAGINATE_BY


class BankAccountListView(LoginRequiredMixin, ListView):
    model = BankAccount
    paginate_by = PAGINATE_BY

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data()
        context['query'] = self.request.GET.get('query', None)

        return context

    def get_queryset(self):
        request = self.request
        query = request.GET.get('query', 'None')

        items = BankAccount.objects.all()
        if query != 'None':
            items = items.filter(Q(bank__name__icontains=query) |
                                 Q(user__username__icontains=query))

        return items


class BankAccountCreateView(LoginRequiredMixin, CreateView):
    model = BankAccount
    success_url = '/madmin/bank-account/list'
    fields = ('bank', 'user', 'account_no', 'branch')


class BankAccountUpdateView(LoginRequiredMixin, UpdateView):
    model = BankAccount
    success_url = '/madmin/bank-account/list'
    fields = ('bank', 'user', 'account_no', 'branch')


class StoreOwnerListView(LoginRequiredMixin, ListView):
    model = StoreOwner
    paginate_by = PAGINATE_BY


class StoreOwnerCreateView(LoginRequiredMixin, CreateView):
    model = StoreOwner
    success_url = '/madmin/store-owner/list'
    fields = ('customer', 'store',)

    def get_initial(self):
        store_id = self.request.GET.get('store', None)
        if store_id:
            store = Store.objects.get(pk=store_id)
            return {"store": store}


class StoreOwnerUpdateView(LoginRequiredMixin, UpdateView):
    model = StoreOwner
    success_url = '/madmin/store-owner/list'
    fields = ('customer', 'store', 'is_active')


class StoreTypeListView(LoginRequiredMixin, ListView):
    model = StoreType
    paginate_by = PAGINATE_BY


class StoreTypeCreateView(LoginRequiredMixin, CreateView):
    model = StoreType
    success_url = '/madmin/store-type/list'
    fields = ('name',)


class StoreTypeUpdateView(LoginRequiredMixin, UpdateView):
    model = StoreType
    success_url = '/madmin/store-type/list'
    fields = ('name',)


class PaymentMethodListView(LoginRequiredMixin, ListView):
    model = PaymentMethod
    paginate_by = PAGINATE_BY


class PaymentMethodCreateView(LoginRequiredMixin, CreateView):
    model = PaymentMethod
    success_url = '/madmin/payment-method/list'
    fields = ('name', 'enabled',)


class PaymentMethodUpdateView(LoginRequiredMixin, UpdateView):
    model = PaymentMethod
    success_url = '/madmin/payment-method/list'
    fields = ('name', 'enabled',)


class AppSettingListView(LoginRequiredMixin, ListView):
    model = AppSetting


class AppSettingCreateView(LoginRequiredMixin, CreateView):
    model = AppSetting
    success_url = '/madmin/app-setting/list'
    fields = ('config', 'val', 'enabled', 'additional_value')


class AppSettingUpdateView(LoginRequiredMixin, UpdateView):
    model = AppSetting
    success_url = '/madmin/app-setting/list'
    fields = ('config', 'val', 'enabled', 'additional_value')


class StorePhotoListView(LoginRequiredMixin, ListView):
    model = StorePhoto
    paginate_by = PAGINATE_BY


class StorePhotoCreateView(LoginRequiredMixin, CreateView):
    model = StorePhoto
    success_url = "/madmin/store-photo/list"
    fields = ('store', 'photo', 'approved',)


class StorePhotoUpdateView(LoginRequiredMixin, UpdateView):
    model = StorePhoto
    success_url = "/madmin/store-photo/list"
    fields = ('store', 'photo', 'approved',)


@login_required
def approve_unapprove_store_photo(request):
    store_photo_id = request.GET.get('store_photo', None)
    action = request.GET.get('action', 'approve')

    try:
        store_photo = StorePhoto.objects.get(pk=store_photo_id)
    except:
        store_photo = None

    if store_photo:
        if action == 'approve':
            store_photo.approved = True
            store_photo.save()
        else:
            store_photo.approved = False
            store_photo.save()

        return redirect("/madmin/store-photo/list")
    else:
        return HttpResponse("Photo not found!")


@login_required
def fav_unfav_store_items(request):
    item_id = request.GET.get('item', None)
    action = request.GET.get('action', 'fav')

    try:
        store_item = StoreItem.objects.get(pk=item_id)
    except:
        store_item = None

    if store_item:
        if action == 'fav':
            store_item.favorite = True
            store_item.save()
        else:
            store_item.favorite = False
            store_item.save()

        return redirect("/madmin/store-item/list")
    else:
        return HttpResponse("Item not found!")


@login_required
def hidden_driver_evaluation(request, driver):
    try:
        driver = Driver.objects.get(pk=driver)
    except:
        driver = None

    if driver:
        if request.method == 'POST':
            message = request.POST.get('message', '')
            if message != '':
                review = HiddenDriverEvaluation()
                review.message = message
                review.driver = driver
                review.save()

        reviews = HiddenDriverEvaluation.objects.all().filter(driver=driver)
        return render(request, 'madmin/hidden_driver_evaluation.html', {'reviews': reviews})

    else:
        return HttpResponse("No driver found!")


@login_required
def send_mass_notifications(request):
    data = {}
    if request.method == "POST":
        message = request.POST.get('message', '')
        if message != '':
            ios_devices = APNSDevice.objects.all()
            ios_devices.send_message(message, sound='default')

            droids = GCMDevice.objects.all()
            droids.send_message(message)

            data['pushed'] = True

    return render(request, 'madmin/send_mass_notifications.html', data)


@login_required
def delete_entry(request):
    model_name = request.GET.get('model')
    pk = request.GET.get('pk')
    model = globals().get(model_name)
    if model:
        model.objects.get(pk=pk).delete()

    messages.info(request, "The item was deleted!")

    return redirect(request.META['HTTP_REFERER'])


@login_required
def user_balance(request):
    if request.method == "POST":
        amount = request.POST.get('amount', '')
        user = request.POST.get('user', '')

        if amount != '' and user != '':
            ub = UserBalance()
            ub.amount = amount
            ub.user = User.objects.get(pk=user)
            ub.given_by = request.user
            ub.save()

        return redirect('/madmin/user/balance?user={}'.format(user))

    user = request.GET.get('user', None)
    balance = None
    if user:
        balance = UserBalance.objects.all().filter(user__id=user).aggregate(balance=Sum('amount'))
        balance = balance.get('balance', None)

    if not balance:
        balance = 0

    return render(request, 'madmin/balance.html', {'balance': balance, 'user': user})


def store_owner_details(request):
    store_id = request.GET.get('store')
    store_owners = StoreOwner.objects.all().filter(store_id=store_id)
    if len(store_owners) > 0:
        store_owner = store_owners[0]
        user_id = store_owner.customer.id
    else:
        user_id = None

    if user_id:
        return redirect("/madmin/details?user={}".format(user_id))


@login_required
def user_details(request):
    user_id = request.GET.get("user", None)
    customer = None
    driver = None
    store_owners = None

    user = User.objects.get(pk=user_id)
    user_locations = UserLocation.objects.all().filter(user=user)
    bank_accounts = BankAccount.objects.all().filter(user=user)
    orders = None

    try:
        customer = user.customer
        orders = Order.objects.all().filter(user=customer)
    except:
        pass

    try:
        driver = user.driver
        orders = Order.objects.all().filter(driver=driver)
    except:
        pass

    if customer:
        store_owners = StoreOwner.objects.all().filter(customer=customer)

    balance = None
    if user:
        balance = UserBalance.objects.all().filter(user__id=user.id).aggregate(balance=Sum('amount'))
        balance = balance.get('balance', None)

    if not balance:
        balance = 0

    data = {
        "user_id": user_id,
        "user_object": user,
        "user_locations": user_locations,
        "bank_accounts": bank_accounts,
        "orders": orders,
        "customer": customer,
        "driver": driver,
        "store_owners": store_owners,
        "balance": balance
    }

    return render(request, "madmin/user_details.html", data)


@login_required
def delete_all_devices(request):
    devices = APNSDevice.objects.all()
    for x in devices:
        x.delete()

    devices = GCMDevice.objects.all()
    for x in devices:
        x.delete()

    return HttpResponse("Ok")
