from django.conf.urls import url
from .views import CustomerListView, CustomerCreateView, CustomerUpdateView, \
    DriverListView, DriverCreateView, DriverUpdateView, UserLocationListView, \
    UserLocationCreateView, UserLocationUpdateView, StoreListView, StoreCreateView, \
    StoreUpdateView, BankListView, BankCreateView, BankUpdateView, AllOrderListView, \
    OrderCreateView, OrderUpdateView, StoreItemListView, StoreItemCreateView, \
    StoreItemUpdateView, StoreBranchCreateView, StoreBranchListView, StoreBranchUpdateView, \
    StoreOwnerListView, StoreOwnerCreateView, StoreOwnerUpdateView, PendingOrderListView, \
    track_order, assign_driver, DriverEvaluationListView, update_order_status, OrderDetailView, \
    BankAccountListView, BankAccountCreateView, BankAccountUpdateView, track_driver, \
    StoreTypeCreateView, StoreTypeListView, StoreTypeUpdateView, PaymentMethodCreateView, \
    PaymentMethodListView, PaymentMethodUpdateView, hidden_driver_evaluation, \
    send_mass_notifications, AppSettingCreateView, AppSettingListView, AppSettingUpdateView, \
    user_balance, StorePhotoListView, StorePhotoCreateView, StorePhotoUpdateView, \
    approve_unapprove_store_photo, fav_unfav_store_items, store_create, user_details, \
    delete_entry, DistrictCreatView, DistrictListView, DistrictUpdateView, \
    madmin_home, store_owner_form, store_owner_details, store_details, delete_all_devices

urlpatterns = [

    url(r'^$', madmin_home, name='madmin_home'),
    url(r'^delete_devices$', delete_all_devices, name='delete_devices'),

    url(r'^store/details$', store_details, name='store_details'),
    url(r'^details$', user_details, name='user_details'),
    url(r'^delete$', delete_entry, name='delete_entry'),

    url(r'^driver/list$', DriverListView.as_view(), name='driver_list'),
    url(r'^driver/create$', DriverCreateView.as_view(), name='driver_create'),
    url(r'^driver/update/(?P<pk>\d+)$', DriverUpdateView.as_view(), name='driver_update'),

    url(r'^notify/all$', send_mass_notifications, name='notify_all'),
    url(r'^driver/track$', track_driver, name='track_driver'),
    url(r'^driver/review/(?P<driver>\d+)$', hidden_driver_evaluation, name='hidden_driver_evaluation'),

    url(r'^order/list$', AllOrderListView.as_view(), name='order_list'),
    url(r'^order/track$', track_order, name='track_order'),
    url(r'^order/assign$', assign_driver, name='assign_driver'),
    url(r'^order/update-status$', update_order_status, name='update_order_status'),
    url(r'^order/list/pending$', PendingOrderListView.as_view(), name='pending_order_list'),
    url(r'^order/create$', OrderCreateView.as_view(), name='order_create'),
    url(r'^order/update/(?P<pk>\d+)$', OrderUpdateView.as_view(), name='order_update'),
    url(r'^order/detail/(?P<pk>\d+)$', OrderDetailView.as_view(), name='order_detail'),

    url(r'^driver-evaluation/list$', DriverEvaluationListView.as_view(), name='driver_evaluation_list'),

    url(r'^customer/list$', CustomerListView.as_view(), name='customer_list'),
    url(r'^customer/create$', CustomerCreateView.as_view(), name='customer_create'),
    url(r'^customer/update/(?P<pk>\d+)$', CustomerUpdateView.as_view(), name='customer_update'),

    url(r'^user/balance$', user_balance, name='user_balance'),
    url(r'^user-location/list$', UserLocationListView.as_view(), name='user_location_list'),
    url(r'^user-location/create$', UserLocationCreateView.as_view(), name='user_location_create'),
    url(r'^user-location/update/(?P<pk>\d+)$', UserLocationUpdateView.as_view(), name='user_location_update'),

    url(r'^store/list$', StoreListView.as_view(), name='store_list'),
    url(r'^store/create$', store_create, name='store_create'),
    url(r'^store/ownerform$', store_owner_form, name='store_owner_form'),
    url(r'^store/update/(?P<pk>\d+)$', StoreUpdateView.as_view(), name='store_update'),

    url(r'^bank/list$', BankListView.as_view(), name='bank_list'),
    url(r'^bank/create$', BankCreateView.as_view(), name='bank_create'),
    url(r'^bank/update/(?P<pk>\d+)$', BankUpdateView.as_view(), name='bank_update'),

    url(r'^district/list$', DistrictListView.as_view(), name='district_list'),
    url(r'^district/create$', DistrictCreatView.as_view(), name='district_create'),
    url(r'^district/update/(?P<pk>\d+)$', DistrictUpdateView.as_view(), name='district_update'),

    url(r'^app-setting/list$', AppSettingListView.as_view(), name='app_setting_list'),
    url(r'^app-setting/create$', AppSettingCreateView.as_view(), name='app_setting_create'),
    url(r'^app-setting/update/(?P<pk>\d+)$', AppSettingUpdateView.as_view(), name='app_setting_update'),

    url(r'^store-type/list$', StoreTypeListView.as_view(), name='store_type_list'),
    url(r'^store-type/create$', StoreTypeCreateView.as_view(), name='store_type_create'),
    url(r'^store-type/update/(?P<pk>\d+)$', StoreTypeUpdateView.as_view(), name='store_type_update'),

    url(r'^payment-method/list$', PaymentMethodListView.as_view(), name='payment_method_list'),
    url(r'^payment-method/create$', PaymentMethodCreateView.as_view(), name='payment_method_create'),
    url(r'^payment-method/update/(?P<pk>\d+)$', PaymentMethodUpdateView.as_view(), name='payment_method_update'),

    url(r'^bank-account/list$', BankAccountListView.as_view(), name='bank_account_list'),
    url(r'^bank-account/create$', BankAccountCreateView.as_view(), name='bank_account_create'),
    url(r'^bank-account/update/(?P<pk>\d+)$', BankAccountUpdateView.as_view(), name='bank_account_update'),

    url(r'^store-photo/fav$', fav_unfav_store_items, name='store_item_fav'),
    url(r'^store-item/list$', StoreItemListView.as_view(), name='store_item_list'),
    url(r'^store-item/create$', StoreItemCreateView.as_view(), name='store_item_create'),
    url(r'^store-item/update/(?P<pk>\d+)$', StoreItemUpdateView.as_view(), name='store_item_update'),

    url(r'^store-branch/list$', StoreBranchListView.as_view(), name='store_branch_list'),
    url(r'^store-branch/create$', StoreBranchCreateView.as_view(), name='store_branch_create'),
    url(r'^store-branch/update/(?P<pk>\d+)$', StoreBranchUpdateView.as_view(), name='store_branch_update'),

    url(r'^store-owner/list$', StoreOwnerListView.as_view(), name='store_owner_list'),
    url(r'^store-owner/create$', StoreOwnerCreateView.as_view(), name='store_owner_create'),
    url(r'^store-owner/update/(?P<pk>\d+)$', StoreOwnerUpdateView.as_view(), name='store_owner_update'),

    url(r'^store-photo/approve$', approve_unapprove_store_photo, name='store_photo_approve'),
    url(r'^store-photo/list$', StorePhotoListView.as_view(), name='store_photo_list'),
    url(r'^store-photo/create$', StorePhotoCreateView.as_view(), name='store_photo_create'),
    url(r'^store-photo/update/(?P<pk>\d+)$', StorePhotoUpdateView.as_view(), name='store_photo_update'),

]
