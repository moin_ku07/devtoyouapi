from django.contrib import admin
from .models import Driver, Customer, UserLocation, \
    Store, StoreBranch, StoreItem, \
    Bank, Order, Payment, OrderEvaluation


class UserLocationInline(admin.StackedInline):
    model = UserLocation
    extra = 0


class CustomerAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if "pbkdf2_sha256" not in obj.password:
            obj.set_password(obj.password)

        super(CustomerAdmin, self).save_model(request, obj, form, change)

    exclude = (
        'is_staff', 'is_active',
        'date_joined', 'groups',
        'user_permissions', 'is_superuser',
        'last_login'
    )
    # inlines = [UserLocationInline]


admin.site.register(Customer, CustomerAdmin)
admin.site.register(UserLocation)


class DriverAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if "pbkdf2_sha256" not in obj.password:
            obj.set_password(obj.password)

        super(DriverAdmin, self).save_model(request, obj, form, change)

    exclude = (
        'is_staff', 'is_active',
        'date_joined', 'groups',
        'user_permissions', 'is_superuser',
        'last_login'
    )


admin.site.register(Driver, DriverAdmin)


class StoreBranchInline(admin.TabularInline):
    model = StoreBranch
    extra = 0


class StoreItemInline(admin.TabularInline):
    model = StoreItem
    extra = 0


class StoreAdmin(admin.ModelAdmin):
    pass  # inlines = [StoreBranchInline, StoreItemInline]


admin.site.register(Store, StoreAdmin)
admin.site.register(StoreItem)
admin.site.register(StoreBranch)

admin.site.register(Bank)


class PaymentInline(admin.StackedInline):
    model = Payment
    extra = 0


class OrderEvaluationInline(admin.StackedInline):
    model = OrderEvaluation
    extra = 0


class OrderAdmin(admin.ModelAdmin):
    pass  # inlines = [PaymentInline, OrderEvaluationInline]


admin.site.register(Order, OrderAdmin)
