# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.db.models import Avg, Sum
from push_notifications.models import APNSDevice, GCMDevice
from django.conf import settings


class SocialToken(models.Model):
    user = models.ForeignKey(User)
    service_name = models.CharField("Service Name", max_length=10)
    profile_id = models.CharField("Profile ID", max_length=60)
    access_token = models.CharField("Access Token", max_length=250)


class Customer(User):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other')
    )
    gender = models.CharField("Gender", max_length=1, choices=GENDER_CHOICES, default='M')
    loyalty_amount = models.FloatField("Loyalty Amount", default=0.00)
    deposit_amount = models.FloatField("Deposit Amount", default=0.00)

    photo = models.ImageField("Photo", blank=True, null=True, upload_to="photos")

    created_at = models.DateTimeField("Created At", auto_now_add=True)
    updated_at = models.DateTimeField("Updated At", auto_now=True)

    def avg_rating(self):
        feedbacks = CustomerEvaluation.objects.all().filter(customer=self).aggregate(Avg('rating'))
        return feedbacks['rating__avg']

    class Meta:
        verbose_name = 'Customer'
        verbose_name_plural = 'Customers'


class Driver(User):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other')
    )
    notes = models.TextField(null=True, blank=True)
    gender = models.CharField("Gender", max_length=1, choices=GENDER_CHOICES, default='M', null=True, blank=True)
    photo = models.ImageField("Photo", blank=True, null=True, upload_to="photos")

    created_at = models.DateTimeField("Created At", auto_now_add=True)
    updated_at = models.DateTimeField("Updated At", auto_now=True)

    def avg_rating(self):
        feedbacks = DriverEvaluation.objects.all().filter(driver=self).aggregate(Avg('rating'))
        return feedbacks['rating__avg']

    def is_available(self):
        orders = Order.objects.filter(driver=self).exclude(status__in=['DELIV', 'MCL', 'UCAN', 'MREJ'])
        if len(orders) > 0:
            return False
        else:
            return True

    class Meta:
        verbose_name = 'Driver'
        verbose_name_plural = 'Drivers'


class HiddenDriverEvaluation(models.Model):
    driver = models.ForeignKey(Driver)
    message = models.CharField("Message", max_length=250, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)


class District(models.Model):
    name = models.CharField("Name", max_length=250)

    def __unicode__(self):
        return self.name


class UserLocation(models.Model):
    user = models.ForeignKey(Customer, related_name='locations')
    name = models.CharField("Name", max_length=250)
    district = models.ForeignKey(District, null=True, blank=True)
    phone = models.CharField("Phone", max_length=250)
    street = models.TextField("Street", null=True, blank=True)
    house_no = models.CharField("House No", max_length=250, null=True, blank=True)
    door_no = models.CharField("Door No", max_length=250, null=True, blank=True)
    door_color = models.CharField("Door Color", max_length=250, null=True, blank=True)
    note = models.TextField("Note", null=True, blank=True)
    longitude = models.CharField("Long", max_length=35)
    latitude = models.CharField("Lat", max_length=35)
    own_location = models.BooleanField("Own Location?", default=True)

    created_at = models.DateTimeField("Created At", auto_now_add=True)
    updated_at = models.DateTimeField("Updated At", auto_now=True)


class StoreType(models.Model):
    name = models.CharField("Store Type", max_length=200)

    def __unicode__(self):
        return self.name


class Store(models.Model):
    name = models.CharField("Store Name", max_length=250)
    thumb = models.ImageField("Thumbnail", null=True, blank=True)

    description = models.TextField("Description", default="N/A")
    avg_time_spent = models.IntegerField("Average Time Spent (Minutes)", default=5)
    store_type = models.ForeignKey(StoreType)
    favorite = models.BooleanField("Favorite", default=False)

    delivery_charge = models.FloatField("Delivery Charge", default=0.0)

    created_at = models.DateTimeField("Created At", auto_now_add=True)
    updated_at = models.DateTimeField("Updated At", auto_now=True)

    def store_balance(self):
        user_balance = UserBalance.objects.all().filter(store=self).aggregate(balance=Sum('amount'))
        return user_balance['balance']

    def thumb_url(self):
        return '%s%s' % (settings.BASE_URL, self.thumb.url)

    def avg_rating(self):
        feedbacks = StoreEvaluation.objects.all().filter(store=self).aggregate(Avg('rating'))
        return feedbacks['rating__avg']

    def __unicode__(self):
        return u"{} - {}".format(self.id, self.name)


class StorePhoto(models.Model):
    store = models.ForeignKey(Store)
    photo = models.ImageField("Store Photo", upload_to="store_photos")
    approved = models.BooleanField("Is approved?", default=False)


class StoreOwner(models.Model):
    customer = models.ForeignKey(Customer)
    store = models.ForeignKey(Store)
    is_active = models.BooleanField("Is Active?", default=True)


class StoreItem(models.Model):
    store = models.ForeignKey(Store, related_name="items")
    name = models.CharField("Item name", max_length=100)
    description = models.TextField("Description")
    photo = models.ImageField("Photo")
    price = models.FloatField("Price")
    favorite = models.BooleanField("Favorite", default=False)

    created_at = models.DateTimeField("Created At", auto_now_add=True)
    updated_at = models.DateTimeField("Updated At", auto_now=True)

    def __unicode__(self):
        return u"{} - {} ".format(self.store.name, self.name)

    def delivery_charge(self):
        return self.store.delivery_charge

    def store_name(self):
        return self.store.name

    def photo_url(self):
        return '%s%s' % (settings.BASE_URL, self.photo.url)

    def avg_rating(self):
        feedbacks = ItemEvaluation.objects.all().filter(item=self).aggregate(Avg('rating'))
        return feedbacks['rating__avg']


class StoreBranch(models.Model):
    store = models.ForeignKey(Store, related_name="branches")
    branch_name = models.CharField("Branch Name", max_length=250)
    district = models.ForeignKey(District, null=True, blank=True)
    phone = models.CharField("Phone", max_length=250)
    longitude = models.CharField("Long", max_length=35)
    latitude = models.CharField("Lat", max_length=35)

    created_at = models.DateTimeField("Created At", auto_now_add=True)
    updated_at = models.DateTimeField("Updated At", auto_now=True)

    def __unicode__(self):
        return u"{} - {} ".format(self.store.name, self.name)


class Bank(models.Model):
    name = models.CharField("Bank Name", max_length=150)

    created_at = models.DateTimeField("Created At", auto_now_add=True)
    updated_at = models.DateTimeField("Updated At", auto_now=True)

    def __unicode__(self):
        return u"{} ".format(self.name)


class BankAccount(models.Model):
    bank = models.ForeignKey(Bank)
    user = models.ForeignKey(User)

    account_no = models.CharField("Account No", max_length=250)
    branch = models.CharField("Branch", max_length=20)

    def __unicode__(self):
        return u"{} {} {}".format(self.bank.name, self.user.username, self.account_no)


class PaymentMethod(models.Model):
    name = models.CharField("Name", max_length=100)
    enabled = models.BooleanField("Is enabled?", default=True)


class Order(models.Model):
    STATUS_CHOICES = (
        ('REQ', u'طلب جديد'),
        ('REV', u'مراجعة'),
        ('APPR', u'معتمد'),
        ('AD', u'مسند إلى المندوب'),
        ('DOTW', u'المندوب في الطريق'),
        ('DIS', u'المندوب في المتجر'),
        ('DLS', u'المندوب غادر المتجر'),
        ('DELIV', u'تم التوصيل'),
        ('MCL', u'مغلق من المشرف'),
        ('UCAN', u'ملغي من العميل'),
        ('MREJ', u'طلب مرفوض')

    )

    PAYMENT_CHOICES = (
        ('COD', 'Cash On Delivery'),
        ('PBC', 'Pay By Credit'),
        ('PBP', 'Pay By Points'),
        ('BT', 'Bank Transfer'),
        ('SPAN', 'Saudi Payment Network')
    )

    user = models.ForeignKey(User)
    driver = models.ForeignKey(Driver, related_name='driver', blank=True, null=True)
    status = models.CharField("Status", max_length=5, choices=STATUS_CHOICES)
    pickup = models.DateTimeField("Requested Pickup Time", null=True, blank=True)
    pickedup = models.DateTimeField("Pickup Time", null=True, blank=True)
    payment_method = models.ForeignKey(PaymentMethod)
    additional_notes = models.TextField(null=True, blank=True)
    total = models.FloatField("Total")
    user_location = models.ForeignKey(UserLocation)
    user_type = models.CharField("User Type", max_length=30)
    paid = models.BooleanField("Paid?", default=True)

    created_at = models.DateTimeField("Created At", auto_now_add=True)
    updated_at = models.DateTimeField("Updated At", auto_now=True)

    def __str__(self):
        return "Order: {}".format(self.id)

    def get_store(self):
        items = self.purchasedstoreitem_set.all()
        if len(items) > 0:
            item = items[0]
            return item.store_item.store
        else:
            return None

    def avg_rating(self):
        feedbacks = OrderEvaluation.objects.all().filter(order=self).aggregate(Avg('rating'))
        return feedbacks['rating__avg']

    def location(self):
        return self.user_location

    def set_store_id(self, store_id):
        self.store_id = store_id

    def display_id(self):
        order = self
        date = order.created_at.date()
        return "{}{}{}{:06}".format(date.year, date.month, date.day, order.id)

    def get_items(self):
        if hasattr(self, 'store_id'):
            return self.purchasedstoreitem_set.all().filter(store_item__store__id=self.store_id)
        else:
            return self.purchasedstoreitem_set.all()

    def save(self, *args, **kw):
        if self.pk is None:
            if not self.paid and self.status == 'APPR':
                if self.payment_method == 'PBC':
                    ub = UserBalance()
                    ub.amount = -(self.total)
                    ub.user = self.user

                    ub.save()



                if self.payment_method == 'PBP':
                    self.user.loyalty_amount = self.user.loyalty_amount - self.total
                    self.user.save()

        if self.pk is not None:
            should_have = []
            for choice in Order.STATUS_CHOICES:
                should_have.append(choice[0])
                if choice[0] == self.status:
                    break

            for status in should_have:
                order_status_change, created = OrderStatusChange.objects.get_or_create(order=self, status=status)
                if created:
                    order_status_change.save()

            to_delete = OrderStatusChange.objects.all().filter(order=self).exclude(status__in=should_have)
            for x in to_delete:
                x.delete()

        super(Order, self).save(*args, **kw)
        send_notifications_order(self)


class PurchasedStoreItem(models.Model):
    order = models.ForeignKey(Order)
    store_item = models.ForeignKey(StoreItem)
    quantity = models.CharField("Quantity", max_length=250)
    price = models.FloatField("Price")
    purchased = models.BooleanField("Is purchased?", default=False)

    def store_owner(self):
        store_owners = StoreOwner.objects.all().filter(store=self.store_item.store)
        if len(store_owners) > 0:
            return store_owners[0].customer.id
        else:
            return None

    def store_id(self):
        return self.store_item.store.id

    def store_name(self):
        return self.store_item.store.name

    def item_name(self):
        return self.store_item.name

    def store_branches(self):
        return self.store_item.store.branches.all()

    def items_feedback(self):
        items = ItemEvaluation.objects.all().filter(order=self.order, item=self.store_item).order_by('-id')
        if len(items) > 0:
            return items[0]
        else:
            return None

    def stores_feedback(self):
        stores = StoreEvaluation.objects.all().filter(order=self.order, store=self.store_item.store).order_by('-id')
        if len(stores):
            return stores
        else:
            return None


class OrderEvaluation(models.Model):
    order = models.ForeignKey(Order)
    user = models.ForeignKey(User)
    rating = models.FloatField("Rating")
    text = models.CharField("Text", max_length=250, null=True, blank=True)


class StoreEvaluation(models.Model):
    order = models.ForeignKey(Order)
    user = models.ForeignKey(User)
    store = models.ForeignKey(Store)
    rating = models.FloatField("Rating")
    text = models.CharField("Text", max_length=250, null=True, blank=True)

    def given_by(self):
        given_by = 'store_owner'
        if self.user.id == self.order.user.id:
            given_by = 'customer'
        if self.user.id == self.order.driver.id:
            given_by = 'driver'

        return given_by


class ItemEvaluation(models.Model):
    order = models.ForeignKey(Order)
    user = models.ForeignKey(User)
    item = models.ForeignKey(StoreItem)
    rating = models.FloatField("Rating")
    text = models.CharField("Text", max_length=250, null=True, blank=True)


class DriverEvaluation(models.Model):
    order = models.ForeignKey(Order)
    user = models.ForeignKey(User, related_name='de_user')
    driver = models.ForeignKey(Driver)
    rating = models.FloatField("Rating")
    text = models.CharField("Text", max_length=250, null=True, blank=True)

    def given_by(self):
        store_owners = StoreOwner.objects.all().filter(customer=self.user)
        if len(store_owners) > 0:
            given_by = "store_owner"
        else:
            given_by = "customer"

        return given_by


class CustomerEvaluation(models.Model):
    order = models.ForeignKey(Order)
    user = models.ForeignKey(User, related_name='ce_user')
    customer = models.ForeignKey(Customer)
    rating = models.FloatField("Rating")
    text = models.CharField("Text", max_length=250, null=True, blank=True)

    def given_by(self):
        given_by = 'store_owner'
        if self.user.id == self.order.driver.id:
            given_by = 'driver'

        return given_by


class OrderStatusChange(models.Model):
    order = models.ForeignKey(Order)
    status = models.CharField("Status", max_length=5, choices=Order.STATUS_CHOICES)
    updated_at = models.DateTimeField(auto_now=True)

    def arabic_status(self):
        status_choices = dict(Order.STATUS_CHOICES)
        return status_choices[self.status]


class Payment(models.Model):
    OPTIONS = (
        ('B', 'Bank'),
        ('DC', 'Delivery Credit'),
        ('COD', 'Cash on Delivery'),
        ('POS', 'Point of Sale'),
        ('FD', 'Free Delivery'),
        ('PA', 'Pay in Advance')

    )

    order = models.ForeignKey(Order)
    option = models.CharField("Option", max_length=5, choices=OPTIONS)
    bank = models.ForeignKey(Bank, null=True, blank=True)
    bank_user_name = models.CharField("Bank Username", max_length=100, null=True, blank=True)
    transfer_id = models.CharField("Transfer ID", max_length=200)
    transfer_copy = models.ImageField("Copy of Transfer")

    created_at = models.DateTimeField("Created At", auto_now_add=True)
    updated_at = models.DateTimeField("Updated At", auto_now=True)


class DriverStoreStatus(models.Model):
    store = models.ForeignKey(Store)
    order = models.ForeignKey(Order)
    status = models.CharField("Status", max_length=5)
    updated_at = models.DateTimeField(auto_now=True)


class DriverGPS(models.Model):
    driver = models.ForeignKey(Driver)
    longitude = models.CharField("Long", max_length=35)
    latitude = models.CharField("Lat", max_length=35)
    updated_at = models.DateTimeField(auto_now=True)


class UserNotification(models.Model):
    user = models.ForeignKey(User)
    message = models.CharField("Message", max_length=250)
    created_at = models.DateTimeField(auto_now_add=True)
    read_at = models.DateTimeField(null=True, blank=True)
    store = models.ForeignKey(Store, null=True, blank=True)


class UserBalance(models.Model):
    user = models.ForeignKey(User, null=True, blank=True)
    store = models.ForeignKey(Store, null=True, blank=True)
    given_by = models.ForeignKey(User, related_name="credited", null=True, blank=True)
    amount = models.FloatField("Amount")


class AppSetting(models.Model):
    config = models.CharField("Key", max_length=50)
    val = models.CharField("Value", max_length=200)
    enabled = models.BooleanField("Enabled", default=True)
    additional_value = models.CharField("Additional Value", max_length=250)


def send_notification(user_id, message, store_id=None, **kwargs):
    user = User.objects.get(pk=user_id)

    if store_id:
        store = Store.objects.get(pk=store_id)
    else:
        store = None

    un = UserNotification()
    un.user = user
    un.message = message

    if store:
        un.store = store

    un.save()

    unread = UserNotification.objects.all().filter(user=user, read_at=None)

    devices = APNSDevice.objects.all().filter(user=user)
    for dev in devices:
        dev.send_message(message, sound='default', badge=len(unread))

    droids = GCMDevice.objects.all().filter(user=user)
    for droid in droids:
        droid.send_message(message)


def send_notifications_order(order):
    user_id = order.user.id if order.user else None
    driver_id = order.driver.id if order.driver else None
    purchased_items = order.purchasedstoreitem_set.all()
    store_owners_list = []
    unique_stores = set()

    for x in purchased_items:
        store = x.store_item.store
        store_owners = StoreOwner.objects.all().filter(store=store)
        if len(store_owners) > 0:
            store_owner = store_owners[0]
        else:
            store_owner = None

        if store.id not in unique_stores:
            if store_owner:
                store_owners_list.append({
                    "owner": store_owner.customer.id,
                    "store": store.id
                })
            unique_stores.add(store.id)

    date = order.created_at.date()
    order_id = "{}{}{}{:06}#".format(date.year, date.month, date.day, order.id)
    message = u"تم تغيير حالة الطلب {} إلى {}  ".format(order_id, order.get_status_display())

    # Send to customer
    if user_id:
        if order.status not in ['REQ', 'UCAN']:
            send_notification(user_id, message)

    # Send to driver
    if driver_id:
        if order.status in ['AD', 'UCAN', 'MCL', 'MREJ']:
            send_notification(driver_id, message)
    # Send to store owners
    for x in store_owners_list:
        if order.status not in ['REQ', 'REV']:
            send_notification(x['owner'], message, store_id=x['store'])
