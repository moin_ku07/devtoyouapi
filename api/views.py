from rest_framework.routers import DefaultRouter
from rest_framework.viewsets import ModelViewSet, ViewSet
from rest_framework import views, status
from rest_framework.response import Response
from datetime import datetime, timedelta
from django.db import models
from push_notifications.api.rest_framework import APNSDeviceAuthorizedViewSet, GCMDeviceAuthorizedViewSet

from .serializers import DriverSerializer, CustomerSerializer, StoreSerializer, StoreItemSerializer, \
    StoreBranchSerializer, DriverEvaluationSerializer, BankSerializer, OrderSerializer, PaymentSerializer, \
    OrderEvaluationSerializer, UserLocationSerializer, OrderStatusChangeSerializer, StoreOwnerSerializer, \
    DriverGPSSerializer, ItemEvaluationSerializer, StoreEvaluationSerializer, UserNotificationSerializer, \
    CustomerEvaluationSerializer, UserBalanceSerializer, StorePhotoSerializer, AppSettingSerializer, \
    DistrictSerializer, PaymentMethodSerializer

from .models import Driver, Customer, Store, StoreItem, StoreBranch, ItemEvaluation, \
    Bank, Order, Payment, OrderEvaluation, UserLocation, PurchasedStoreItem, OrderStatusChange, \
    StoreOwner, DriverStoreStatus, DriverGPS, DriverEvaluation, StoreEvaluation, UserNotification, \
    CustomerEvaluation, UserBalance, StorePhoto, AppSetting, District, PaymentMethod


class CustomerViewSet(ModelViewSet):
    serializer_class = CustomerSerializer
    queryset = Customer.objects.all()


class DistrictViewSet(ModelViewSet):
    serializer_class = DistrictSerializer
    queryset = District.objects.all()


class DriverViewSet(ModelViewSet):
    serializer_class = DriverSerializer
    queryset = Driver.objects.all()


class StoreViewSet(ModelViewSet):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer

    def get_queryset(self):
        favorite = self.request.GET.get('favorite', None)

        stores = Store.objects.all()

        if favorite:
            stores = stores.filter(favorite=True)

        return stores


class StorePhotoViewSet(ModelViewSet):
    queryset = StorePhoto.objects.all()
    serializer_class = StorePhotoSerializer

    def get_queryset(self):
        store = self.request.GET.get('store', None)

        store_photos = StorePhoto.objects.all().filter(approved=True)

        if store:
            store_photos = store_photos.filter(store=store)

        return store_photos


class BankViewSet(ModelViewSet):
    queryset = Bank.objects.all()
    serializer_class = BankSerializer


class OrderViewSet(ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def get_object(self):
        obj = super(OrderViewSet, self).get_object()
        store_id = self.request.GET.get('store', None)
        if store_id:
            obj.set_store_id(store_id)
        return obj

    def get_queryset(self):
        request = self.request
        user_id = request.GET.get('user', None)
        store_id = request.GET.get('store', None)
        driver_id = request.GET.get('driver', None)
        exclude_delivered = request.GET.get('exclude_delivered', 'No')
        exclude_cancelled = request.GET.get('exclude_cancelled', 'No')
        include_user = request.GET.get('include_user', 'No')

        orders = Order.objects.all().order_by('-created_at')

        if exclude_delivered != 'No':
            orders = orders.exclude(status='DELIV')

        if exclude_cancelled != 'No':
            orders = orders.exclude(status='UCAN')

        if user_id:
            orders = orders.filter(user__id=user_id)

        if driver_id:
            orders = orders.filter(driver__id=driver_id)

        if self.action == 'list':
            if store_id:
                purchased_items = PurchasedStoreItem.objects.all().filter(store_item__store__id=store_id)
                orders = set()
                for pi in purchased_items:
                    order = pi.order
                    order.set_store_id(store_id)
                    if exclude_delivered != 'No':
                        if order.status != 'DELIV':
                            orders.add(order)
                    else:
                        orders.add(order)

                orders = sorted(orders, key=lambda k: k.created_at)
                orders.reverse()

        if include_user != 'No':
            user_orders = Order.objects.filter(user__id=include_user)
            orders = list(orders)
            for x in user_orders:
                orders.append(x)

        return orders

    def create(self, request, *args, **kwargs):
        items = request.DATA.get('items', None)
        delivery_address = request.DATA.get('delivery_address', None)
        user = request.DATA.get('user', None)

        user_type = "customer"

        if delivery_address:
            user_location = UserLocation()
            user_location.user = Customer.objects.get(pk=user)
            user_location.region = delivery_address['region']
            user_location.phone = delivery_address['phone']
            user_location.street = delivery_address['street']
            user_location.house_no = delivery_address['house_no']
            user_location.door_no = delivery_address['door_no']
            user_location.door_color = delivery_address['door_color']
            user_location.name = delivery_address['name']
            user_location.longitude = delivery_address['gps']['lng']
            user_location.latitude = delivery_address['gps']['lat']

            user_location.own_location = False

            user_location.save()

            location_id = user_location.id

            request.DATA['user_location'] = location_id

            user_type = "store_owner"

        request.DATA['user_type'] = user_type

        order_serializer = OrderSerializer(data=request.DATA)
        if order_serializer.is_valid():
            try:
                order = order_serializer.create(order_serializer.validated_data)

                order_status, _ = OrderStatusChange.objects.get_or_create(order=order, status=order.status)
                order_status.save()

                if order:
                    if items:
                        for item in items:
                            purchased_item = PurchasedStoreItem()
                            purchased_item.store_item_id = item['store_item']
                            purchased_item.quantity = item['quantity']
                            purchased_item.price = item['price']
                            purchased_item.order = order
                            purchased_item.save()

                    return Response({"status": True, 'order_id': order.id, "display_id": order.display_id()})
                else:
                    return Response({"status": False, "message": "Order was not created"})
            except Exception as ex:
                return Response({"status": False, "error": str(ex)})
        else:
            return Response({"status": False, "msg": "Serializer invalid", "errors": order_serializer.errors})


### Related ones

class StoreItemViewSet(ModelViewSet):
    queryset = StoreItem.objects.all()
    serializer_class = StoreItemSerializer

    def get_queryset(self):
        request = self.request
        store_items = StoreItem.objects.all()

        store_id = request.GET.get('store', None)
        if store_id:
            store_items = store_items.filter(store__id=store_id)

        favorite = request.GET.get('favorite', None)
        if favorite:
            store_items = store_items.filter(favorite=True)

        return store_items


class StoreBranchViewSet(ModelViewSet):
    queryset = StoreBranch.objects.all()
    serializer_class = StoreBranchSerializer

    def get_queryset(self):
        request = self.request

        store_id = request.GET.get('store', None)
        if store_id:
            return StoreBranch.objects.all().filter(store__id=store_id)


class PaymentViewSet(ModelViewSet):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer


class OrderEvaluationViewSet(ModelViewSet):
    queryset = OrderEvaluation.objects.all()
    serializer_class = OrderEvaluationSerializer


class ItemEvaluationViewSet(ModelViewSet):
    queryset = ItemEvaluation.objects.all()
    serializer_class = ItemEvaluationSerializer


class DriverEvaluationViewSet(ModelViewSet):
    queryset = DriverEvaluation.objects.all()
    serializer_class = DriverEvaluationSerializer


class CustomerEvaluationViewSet(ModelViewSet):
    queryset = CustomerEvaluation.objects.all()
    serializer_class = CustomerEvaluationSerializer


class StoreEvaluationViewSet(ModelViewSet):
    queryset = StoreEvaluation.objects.all()
    serializer_class = StoreEvaluationSerializer


class UserLocationViewSet(ModelViewSet):
    queryset = UserLocation.objects.all()
    serializer_class = UserLocationSerializer

    def get_queryset(self):
        return UserLocation.objects.all().filter(own_location=True)


class StoreOwnerViewSet(ModelViewSet):
    queryset = StoreOwner.objects.all()
    serializer_class = StoreOwnerSerializer


class DriverGPSViewSet(ModelViewSet):
    queryset = DriverGPS.objects.all()
    serializer_class = DriverGPSSerializer


class UserBalanceViewSet(ModelViewSet):
    queryset = UserBalance.objects.all()
    serializer_class = UserBalanceSerializer


class UserNotificationViewSet(ModelViewSet):
    queryset = UserNotification.objects.all()
    serializer_class = UserNotificationSerializer

    def get_queryset(self):
        request = self.request
        user = request.GET.get('user', None)
        store = request.GET.get('store', None)

        notifications = UserNotification.objects.all().order_by('-created_at')
        if user:
            notifications = notifications.filter(user__id=user)
        if store:
            notifications = notifications.filter(store__id=store)

        return notifications


class PaymentMethodViewSet(ModelViewSet):
    queryset = PaymentMethod.objects.all()
    serializer_class = PaymentMethodSerializer


class AppSettingViewSet(ModelViewSet):
    queryset = AppSetting.objects.all()
    serializer_class = AppSettingSerializer


class TrackOrder(views.APIView):
    def get(self, request):
        order_id = request.GET.get('order', None)
        try:
            order = Order.objects.get(pk=order_id)
        except Exception as ex:
            order = None

        if order:
            data = {}
            data['status'] = order.status

            data['updated_at'] = order.updated_at

            order_status_changes = OrderStatusChange.objects.all().filter(order=order)
            order_status_changes_serializer = OrderStatusChangeSerializer(data=order_status_changes, many=True)
            order_status_changes_serializer.is_valid()

            changes = order_status_changes_serializer.data

            data['changes'] = changes

            start_time = False
            last_time = datetime.now()
            for x in order_status_changes:
                if x.status == 'AD':
                    start_time = x.updated_at

                if x.status == 'DELIV':
                    last_time = x.updated_at

            location = []
            print start_time
            if start_time:
                driver_gps = DriverGPS.objects.all() \
                    .filter(driver=order.driver, updated_at__range=(start_time, last_time)) \
                    .order_by('-updated_at')

                for loc in driver_gps:
                    location.append({"lng": loc.longitude, "lat": loc.latitude})

            data['driver_location'] = location

            return Response(data)
        else:
            return Response({"status": False, "message": "Order does not exist"})


class UpdateOrderStatus(views.APIView):
    def post(self, request):
        order_id = request.DATA.get('order', None)
        store_id = request.DATA.get('store', None)
        status = request.DATA.get('status', None)

        res = {"message": "Order was not found!", "status": False}

        if order_id and store_id and status:
            order = Order.objects.get(pk=order_id)
            store = Store.objects.get(pk=store_id)

            driver_store_status = DriverStoreStatus.objects.all().filter(order=order, store=store)
            if len(driver_store_status) > 0:
                dss = driver_store_status[0]
                dss.status = status
                dss.save()
            else:
                dss = DriverStoreStatus(order=order, store=store, status=status)
                dss.save()

            if status == 'DIS':
                order.status = 'DIS'
                left_stores = DriverStoreStatus.objects.all().filter(order=order).exclude(store=store)
                for store in left_stores:
                    store.status = 'DLS'
                    store.save()

            else:
                if order.status == 'DIS' and status == 'DLS':
                    purchased_items = order.purchasedstoreitem_set.all()
                    for x in purchased_items:
                        if int(store_id) == x.store_item.store_id:
                            x.purchased = True
                            x.save()

                    DLS = True
                    driver_stores = DriverStoreStatus.objects.all().filter(order=order)
                    for x in driver_stores:
                        if x.status != 'DLS':
                            DLS = False

                    if DLS:
                        order.status = 'DLS'

            order.save()
            res['message'] = "Current Status:  " + order.status
            res['status'] = True

        return Response(res)


class SearchView(views.APIView):
    def get(self, request):
        search_query = request.GET.get('query', None)
        results = []
        if search_query:
            fields = [f for f in Store._meta.fields if
                      isinstance(f, models.CharField) or isinstance(f, models.TextField)]
            queries = [models.Q(**{f.name + "__icontains": search_query}) for f in fields]
            qs = models.Q()
            for query in queries:
                qs = qs | query

            stores = Store.objects.filter(qs)

            for store in stores:
                serialized_store = StoreSerializer(store, context={"request": request})
                data = serialized_store.data
                data['type'] = 'store'
                results.append(data)

            fields = [f for f in StoreItem._meta.fields if
                      isinstance(f, models.CharField) or isinstance(f, models.TextField)]
            queries = [models.Q(**{f.name + "__icontains": search_query}) for f in fields]

            qs = models.Q()
            for query in queries:
                qs = qs | query

            store_items = StoreItem.objects.filter(qs)
            for store_item in store_items:
                serialized_store_item = StoreItemSerializer(store_item, context={"request": request})
                data = serialized_store_item.data
                data['type'] = 'store_item'
                results.append(data)

        return Response(results)


class MarkNotificationsRead(views.APIView):
    def post(self, request):
        data = {"status": False, "message": ""}
        user_id = request.DATA.get('user', None)
        if user_id:
            notifications = UserNotification.objects.all().filter(read_at=None)
            if len(notifications) > 0:
                for notif in notifications:
                    notif.read_at = datetime.now()
                    notif.save()

                data['status'] = True
                data['message'] = "Notifications marked as read"

            else:
                data['message'] = "No unread notifications found!"

        else:
            data['message'] = "No user found!"

        return Response(data)


class StatsView(views.APIView):
    def get(self, request):
        user_id = request.GET.get('user', None)
        user_type = request.GET.get('type', 'customer')

        periods = ['day', 'week', 'month', 'all']
        stats = {}

        for period in periods:
            data = self.get_stats(user_id, user_type, period)
            stats[period] = data

        if user_type == 'customer':
            customer = Customer.objects.get(pk=user_id)
            stats['points'] = customer.loyalty_amount
            stats['credits'] = customer.deposit_amount

        return Response(stats)

    def get_stats(self, user_id, user_type, period):

        time_period = datetime.now().replace(hour=0, minute=0, second=0)
        if period == 'week':
            time_period = datetime.now() - timedelta(days=7)
        if period == 'month':
            time_period = datetime.now() - timedelta(days=30)
        if period == 'all':
            time_period = datetime(1970, 1, 1)

        if user_type == 'customer':
            pending_orders = Order.objects.filter(user__id=user_id, created_at__gte=time_period,
                                                  status__in=['REQ', 'REV'])

            accepted_orders = Order.objects.filter(user__id=user_id, created_at__gte=time_period,
                                                   status__in=['APPR', 'AD'])

            on_the_way_orders = Order.objects.filter(user__id=user_id, created_at__gte=time_period,
                                                     status__in=['DOTW', 'DIS', 'DLS'])

            delivered_orders = Order.objects.filter(user__id=user_id, created_at__gte=time_period,
                                                    status__in=['DELIV'])

        elif user_type == 'driver':
            pending_orders = Order.objects.filter(driver__id=user_id, created_at__gte=time_period,
                                                  status__in=['REQ', 'REV'])

            accepted_orders = Order.objects.filter(driver__id=user_id, created_at__gte=time_period,
                                                   status__in=['APPR', 'AD'])

            on_the_way_orders = Order.objects.filter(driver__id=user_id, created_at__gte=time_period,
                                                     status__in=['DOTW', 'DIS', 'DLS'])

            delivered_orders = Order.objects.filter(driver__id=user_id, created_at__gte=time_period,
                                                    status__in=['DELIV'])
        else:
            store_owners = StoreOwner.objects.all().filter(customer__id=user_id)
            if len(store_owners) > 0:
                store_owner = store_owners[0]
            else:
                store_owner = None

            if store_owner:
                store_id = store_owner.store.id
                purchased_items = PurchasedStoreItem.objects.all().filter(store_item__store__id=store_id)
                orders = set()
                for item in purchased_items:
                    orders.add(item.order)

                pending_orders = [order for order in orders if order.status in ['REQ', 'REV']]
                accepted_orders = [order for order in orders if order.status in ['APPR', 'AD']]
                on_the_way_orders = [order for order in orders if order.status in ['DOTW', 'DIS', 'DLS']]
                delivered_orders = [order for order in orders if order.status in ['DELIV']]


            else:
                pending_orders = []
                accepted_orders = []
                on_the_way_orders = []
                delivered_orders = []

        pending_orders_count = len(pending_orders)
        accepted_orders_count = len(accepted_orders)
        on_the_way_orders_count = len(on_the_way_orders)
        delivered_orders_count = len(delivered_orders)

        total = float(pending_orders_count + accepted_orders_count + on_the_way_orders_count + delivered_orders_count)
        if total == 0:
            stats = {
                'pending': 0,
                'accepted': 0,
                'on_the_way': 0,
                'delivered': 0,

            }
        else:
            stats = {
                'pending': (float(pending_orders_count) / total) * 100,
                'accepted': (float(accepted_orders_count) / total) * 100,
                'on_the_way': (float(on_the_way_orders_count) / total) * 100,
                'delivered': (float(delivered_orders_count) / total) * 100,

            }

        return stats


from push_notifications.models import APNSDevice, GCMDevice


class CustomAPNSDeviceAuthorizedViewSet(APNSDeviceAuthorizedViewSet):
    def create(self, request, *args, **kwargs):
        registration_id = request.DATA.get('registration_id')
        if registration_id:
            devices = APNSDevice.objects.filter(registration_id=registration_id)
            for x in devices:
                x.delete()

        return super(CustomAPNSDeviceAuthorizedViewSet, self).create(request, args, kwargs)


class CustomGCMDeviceAuthorizedViewSet(GCMDeviceAuthorizedViewSet):
    def create(self, request, *args, **kwargs):
        registration_id = request.DATA.get('registration_id')
        if registration_id:
            devices = GCMDevice.objects.filter(registration_id=registration_id)
            for x in devices:
                x.delete()

        return super(CustomGCMDeviceAuthorizedViewSet, self).create(request, args, kwargs)


router = DefaultRouter()

router.register('drivers', DriverViewSet)
router.register('districts', DistrictViewSet)
router.register('customers', CustomerViewSet)
router.register('stores', StoreViewSet)
router.register('banks', BankViewSet)
router.register('orders', OrderViewSet),
router.register('driver-gps', DriverGPSViewSet),
router.register('user-notification', UserNotificationViewSet),
router.register(r'device/apns', CustomAPNSDeviceAuthorizedViewSet)
router.register(r'device/gcm', CustomGCMDeviceAuthorizedViewSet)
router.register('app-settings', AppSettingViewSet),

### related
router.register('store-owners', StoreOwnerViewSet),
router.register('store-items', StoreItemViewSet)
router.register('store-photos', StorePhotoViewSet)
router.register('user-locations', UserLocationViewSet)
router.register('store-branches', StoreBranchViewSet)
router.register('payments', PaymentViewSet)
router.register('payment-method', PaymentMethodViewSet)
router.register('order-evaluations', OrderEvaluationViewSet)
router.register('item-evaluations', ItemEvaluationViewSet)
router.register('driver-evaluations', DriverEvaluationViewSet)
router.register('store-evaluations', StoreEvaluationViewSet)
router.register('customer-evaluations', CustomerEvaluationViewSet)
router.register('user-balance', UserBalanceViewSet)
