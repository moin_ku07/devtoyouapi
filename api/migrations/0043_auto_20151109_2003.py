# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('api', '0042_hiddendriverevaluation'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserBalance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.FloatField(verbose_name=b'Amount')),
                ('given_by', models.ForeignKey(related_name='credited', to=settings.AUTH_USER_MODEL)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(max_length=5, verbose_name=b'Status', choices=[(b'REQ', '\u0637\u0644\u0628 \u062c\u062f\u064a\u062f'), (b'REV', '\u0645\u0631\u0627\u062c\u0639\u0629'), (b'APPR', '\u0645\u0639\u062a\u0645\u062f'), (b'AD', '\u0645\u0633\u0646\u062f \u0625\u0644\u0649 \u0627\u0644\u0645\u0646\u062f\u0648\u0628'), (b'DOTW', '\u0627\u0644\u0645\u0646\u062f\u0648\u0628 \u0641\u064a \u0627\u0644\u0637\u0631\u064a\u0642'), (b'DIS', '\u0627\u0644\u0645\u0646\u062f\u0648\u0628 \u0641\u064a \u0627\u0644\u0645\u062a\u062c\u0631'), (b'DLS', '\u0627\u0644\u0645\u0646\u062f\u0648\u0628 \u063a\u0627\u062f\u0631 \u0627\u0644\u0645\u062a\u062c\u0631'), (b'DELIV', '\u062a\u0645 \u0627\u0644\u062a\u0648\u0635\u064a\u0644'), (b'MCL', '\u0645\u063a\u0644\u0642 \u0645\u0646 \u0627\u0644\u0645\u0634\u0631\u0641'), (b'UCAN', '\u0645\u0644\u063a\u064a \u0645\u0646 \u0627\u0644\u0639\u0645\u064a\u0644'), (b'MREJ', '\u0637\u0644\u0628 \u0645\u0631\u0641\u0648\u0636')]),
        ),
        migrations.AlterField(
            model_name='orderstatuschange',
            name='status',
            field=models.CharField(max_length=5, verbose_name=b'Status', choices=[(b'REQ', '\u0637\u0644\u0628 \u062c\u062f\u064a\u062f'), (b'REV', '\u0645\u0631\u0627\u062c\u0639\u0629'), (b'APPR', '\u0645\u0639\u062a\u0645\u062f'), (b'AD', '\u0645\u0633\u0646\u062f \u0625\u0644\u0649 \u0627\u0644\u0645\u0646\u062f\u0648\u0628'), (b'DOTW', '\u0627\u0644\u0645\u0646\u062f\u0648\u0628 \u0641\u064a \u0627\u0644\u0637\u0631\u064a\u0642'), (b'DIS', '\u0627\u0644\u0645\u0646\u062f\u0648\u0628 \u0641\u064a \u0627\u0644\u0645\u062a\u062c\u0631'), (b'DLS', '\u0627\u0644\u0645\u0646\u062f\u0648\u0628 \u063a\u0627\u062f\u0631 \u0627\u0644\u0645\u062a\u062c\u0631'), (b'DELIV', '\u062a\u0645 \u0627\u0644\u062a\u0648\u0635\u064a\u0644'), (b'MCL', '\u0645\u063a\u0644\u0642 \u0645\u0646 \u0627\u0644\u0645\u0634\u0631\u0641'), (b'UCAN', '\u0645\u0644\u063a\u064a \u0645\u0646 \u0627\u0644\u0639\u0645\u064a\u0644'), (b'MREJ', '\u0637\u0644\u0628 \u0645\u0631\u0641\u0648\u0636')]),
        ),
    ]
