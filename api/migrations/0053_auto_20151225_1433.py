# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0052_storeowner_is_active'),
    ]

    operations = [
        migrations.AlterField(
            model_name='driver',
            name='notes',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='storeowner',
            name='is_active',
            field=models.BooleanField(default=True, verbose_name=b'Is Active?'),
        ),
    ]
