# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0026_driverstorestatus'),
    ]

    operations = [
        migrations.CreateModel(
            name='DriverGPS',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('longitude', models.CharField(max_length=20, verbose_name=b'Long')),
                ('latitude', models.CharField(max_length=20, verbose_name=b'Lat')),
                ('updated_at', models.DateTimeField(auto_now_add=True)),
                ('driver', models.ForeignKey(to='api.Driver')),
            ],
        ),
    ]
