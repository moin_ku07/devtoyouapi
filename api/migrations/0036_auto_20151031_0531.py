# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0035_bankaccount'),
    ]

    operations = [
        migrations.AddField(
            model_name='store',
            name='favorite',
            field=models.BooleanField(default=False, verbose_name=b'Favorite'),
        ),
        migrations.AddField(
            model_name='storeitem',
            name='favorite',
            field=models.BooleanField(default=False, verbose_name=b'Favorite'),
        ),
        migrations.AlterField(
            model_name='store',
            name='store_type',
            field=models.CharField(max_length=3, verbose_name=b'Store Type', choices=[(b'R', b'Restaurant'), (b'H', b'Home Store'), (b'S', b'Sweet Store'), (b'O', b'Other')]),
        ),
    ]
