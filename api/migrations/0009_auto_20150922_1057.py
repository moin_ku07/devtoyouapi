# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0008_remove_customer_primary_phone'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='photo',
            field=models.ImageField(null=True, verbose_name='Photo', blank=True, upload_to='photos'),
        ),
        migrations.AlterField(
            model_name='driver',
            name='photo',
            field=models.ImageField(null=True, verbose_name='Photo', blank=True, upload_to='photos'),
        ),
        migrations.AlterField(
            model_name='userlocation',
            name='door_color',
            field=models.CharField(null=True, verbose_name='Door Color', blank=True, max_length=250),
        ),
        migrations.AlterField(
            model_name='userlocation',
            name='door_no',
            field=models.CharField(null=True, verbose_name='Door No', blank=True, max_length=250),
        ),
        migrations.AlterField(
            model_name='userlocation',
            name='house_no',
            field=models.CharField(null=True, verbose_name='House No', blank=True, max_length=250),
        ),
        migrations.AlterField(
            model_name='userlocation',
            name='note',
            field=models.TextField(null=True, verbose_name='Note', blank=True),
        ),
        migrations.AlterField(
            model_name='userlocation',
            name='street',
            field=models.TextField(null=True, verbose_name='Street', blank=True),
        ),
    ]
