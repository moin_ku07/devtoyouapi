# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0021_auto_20151009_1028'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderStatusChange',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(max_length=5, verbose_name=b'Status', choices=[(b'REQ', b'Request'), (b'REV', b'Review'), (b'APPR', b'Approved'), (b'AD', b'Assigned to Driver'), (b'DOTW', b'Driver On The Way'), (b'DIS', b'Driver In The Store'), (b'DLS', b'Driver Left Store'), (b'DELIV', b'Delivered'), (b'MCL', b'Moderator Closed'), (b'UCAN', b'User Cancelled'), (b'MREJ', b'Moderator Rejected Order')])),
                ('updated_at', models.DateTimeField(auto_now_add=True)),
                ('order', models.ForeignKey(to='api.Order')),
            ],
        ),
    ]
