# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0048_storephoto_approved'),
    ]

    operations = [
        migrations.AlterField(
            model_name='store',
            name='avg_time_spent',
            field=models.IntegerField(default=5, verbose_name=b'Average Time Spent (Minutes)'),
        ),
        migrations.AlterField(
            model_name='store',
            name='description',
            field=models.TextField(default=b'N/A', verbose_name=b'Description'),
        ),
        migrations.AlterField(
            model_name='store',
            name='thumb',
            field=models.ImageField(upload_to=b'', null=True, verbose_name=b'Thumbnail', blank=True),
        ),
    ]
