# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0037_storetype'),
    ]

    operations = [
        migrations.AlterField(
            model_name='store',
            name='store_type',
            field=models.ForeignKey(to='api.StoreType'),
        ),
    ]
