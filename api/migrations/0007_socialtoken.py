# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('api', '0006_userlocation_door_no'),
    ]

    operations = [
        migrations.CreateModel(
            name='SocialToken',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('service_name', models.CharField(verbose_name='Service Name', max_length=10)),
                ('profile_id', models.CharField(verbose_name='Profile ID', max_length=60)),
                ('access_token', models.CharField(verbose_name='Access Token', max_length=250)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
