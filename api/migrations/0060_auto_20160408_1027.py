# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0059_auto_20160408_0858'),
    ]

    operations = [
        migrations.AlterField(
            model_name='driverevaluation',
            name='text',
            field=models.CharField(max_length=250, null=True, verbose_name=b'Text', blank=True),
        ),
    ]
