# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0050_auto_20151128_1923'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='pickedup',
            field=models.DateTimeField(null=True, verbose_name=b'Pickup Time', blank=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='pickup',
            field=models.DateTimeField(null=True, verbose_name=b'Requested Pickup Time', blank=True),
        ),
    ]
