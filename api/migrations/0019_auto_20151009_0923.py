# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0018_purchasedstoreitem_purchased'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='store',
        ),
        migrations.AlterField(
            model_name='order',
            name='pickup',
            field=models.DateTimeField(null=True, verbose_name=b'Pick Up Time', blank=True),
        ),
    ]
