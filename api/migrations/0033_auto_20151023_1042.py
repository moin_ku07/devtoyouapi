# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0032_auto_20151023_0752'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomerEvaluation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.FloatField(verbose_name=b'Rating')),
                ('text', models.CharField(max_length=250, verbose_name=b'Text')),
                ('customer', models.ForeignKey(to='api.Customer')),
                ('order', models.ForeignKey(to='api.Order')),
            ],
        ),
        migrations.AlterField(
            model_name='driverevaluation',
            name='rating',
            field=models.FloatField(verbose_name=b'Rating'),
        ),
        migrations.AlterField(
            model_name='itemevaluation',
            name='rating',
            field=models.FloatField(verbose_name=b'Rating'),
        ),
        migrations.AlterField(
            model_name='orderevaluation',
            name='rating',
            field=models.FloatField(verbose_name=b'Rating'),
        ),
        migrations.AlterField(
            model_name='storeevaluation',
            name='rating',
            field=models.FloatField(verbose_name=b'Rating'),
        ),
    ]
