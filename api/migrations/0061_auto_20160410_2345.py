# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0060_auto_20160408_1027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customerevaluation',
            name='text',
            field=models.CharField(max_length=250, null=True, verbose_name=b'Text', blank=True),
        ),
        migrations.AlterField(
            model_name='hiddendriverevaluation',
            name='message',
            field=models.CharField(max_length=250, null=True, verbose_name=b'Message', blank=True),
        ),
        migrations.AlterField(
            model_name='itemevaluation',
            name='text',
            field=models.CharField(max_length=250, null=True, verbose_name=b'Text', blank=True),
        ),
        migrations.AlterField(
            model_name='storeevaluation',
            name='text',
            field=models.CharField(max_length=250, null=True, verbose_name=b'Text', blank=True),
        ),
    ]
