# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0022_orderstatuschange'),
    ]

    operations = [
        migrations.CreateModel(
            name='StoreOwner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('customer', models.ForeignKey(to='api.Customer')),
                ('store', models.ForeignKey(to='api.Store')),
            ],
        ),
    ]
