# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0025_auto_20151020_0819'),
    ]

    operations = [
        migrations.CreateModel(
            name='DriverStoreStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(max_length=5, verbose_name=b'Status')),
                ('updated_at', models.DateTimeField(auto_now_add=True)),
                ('order', models.ForeignKey(to='api.Order')),
                ('store', models.ForeignKey(to='api.Store')),
            ],
        ),
    ]
