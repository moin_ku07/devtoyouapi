# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0058_auto_20160405_1232'),
    ]

    operations = [
        migrations.RenameField(
            model_name='storebranch',
            old_name='name',
            new_name='branch_name',
        ),
    ]
