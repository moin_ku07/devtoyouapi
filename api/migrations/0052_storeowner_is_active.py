# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0051_auto_20151210_1658'),
    ]

    operations = [
        migrations.AddField(
            model_name='storeowner',
            name='is_active',
            field=models.BooleanField(default=True, verbose_name=b'Is Active?'),
            preserve_default=False,
        ),
    ]
