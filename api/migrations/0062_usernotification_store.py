# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0061_auto_20160410_2345'),
    ]

    operations = [
        migrations.AddField(
            model_name='usernotification',
            name='store',
            field=models.ForeignKey(blank=True, to='api.Store', null=True),
        ),
    ]
