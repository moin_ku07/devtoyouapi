# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0029_remove_drivergps_order'),
    ]

    operations = [
        migrations.CreateModel(
            name='DriverEvaluation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.IntegerField(verbose_name=b'Rating')),
                ('text', models.CharField(max_length=250, verbose_name=b'Text')),
                ('driver', models.ForeignKey(to='api.Driver')),
                ('order', models.ForeignKey(to='api.Order')),
            ],
        ),
        migrations.CreateModel(
            name='ItemEvaluation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.IntegerField(verbose_name=b'Rating')),
                ('text', models.CharField(max_length=250, verbose_name=b'Text')),
                ('item', models.ForeignKey(to='api.StoreItem')),
                ('order', models.ForeignKey(to='api.Order')),
            ],
        ),
        migrations.RemoveField(
            model_name='orderevaluation',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='orderevaluation',
            name='driver_evalutation',
        ),
        migrations.RemoveField(
            model_name='orderevaluation',
            name='driver_rating',
        ),
        migrations.RemoveField(
            model_name='orderevaluation',
            name='moderator_evalutation',
        ),
        migrations.RemoveField(
            model_name='orderevaluation',
            name='moderator_rating',
        ),
        migrations.RemoveField(
            model_name='orderevaluation',
            name='order_evalutation',
        ),
        migrations.RemoveField(
            model_name='orderevaluation',
            name='order_rating',
        ),
        migrations.RemoveField(
            model_name='orderevaluation',
            name='updated_at',
        ),
        migrations.AddField(
            model_name='orderevaluation',
            name='rating',
            field=models.IntegerField(default=1, verbose_name=b'Rating'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='orderevaluation',
            name='text',
            field=models.CharField(default='N/A', max_length=250, verbose_name=b'Text'),
            preserve_default=False,
        ),
    ]
