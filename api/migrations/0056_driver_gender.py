# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0055_auto_20160101_1553'),
    ]

    operations = [
        migrations.AddField(
            model_name='driver',
            name='gender',
            field=models.CharField(default=b'M', choices=[(b'M', b'Male'), (b'F', b'Female'), (b'O', b'Other')], max_length=1, blank=True, null=True, verbose_name=b'Gender'),
        ),
    ]
