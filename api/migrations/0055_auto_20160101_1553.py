# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0054_auto_20151226_0437'),
    ]

    operations = [
        migrations.CreateModel(
            name='District',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250, verbose_name=b'Name')),
            ],
        ),
        migrations.RemoveField(
            model_name='storebranch',
            name='region',
        ),
        migrations.RemoveField(
            model_name='userlocation',
            name='region',
        ),
        migrations.AddField(
            model_name='storebranch',
            name='district',
            field=models.ForeignKey(blank=True, to='api.District', null=True),
        ),
        migrations.AddField(
            model_name='userlocation',
            name='district',
            field=models.ForeignKey(blank=True, to='api.District', null=True),
        ),
    ]
