# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0013_order_items'),
    ]

    operations = [
        migrations.CreateModel(
            name='PurchasedStoreItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.CharField(max_length=250, verbose_name=b'Quantity')),
                ('store_item', models.ForeignKey(to='api.StoreItem')),
            ],
        ),
        migrations.AlterField(
            model_name='order',
            name='items',
            field=models.ManyToManyField(to='api.PurchasedStoreItem'),
        ),
    ]
