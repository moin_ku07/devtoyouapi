# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0053_auto_20151225_1433'),
    ]

    operations = [
        migrations.AddField(
            model_name='appsetting',
            name='additional_value',
            field=models.CharField(default='N/A', max_length=250, verbose_name=b'Additional Value'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='appsetting',
            name='enabled',
            field=models.BooleanField(default=True, verbose_name=b'Enabled'),
        ),
    ]
