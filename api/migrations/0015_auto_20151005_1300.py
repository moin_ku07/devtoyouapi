# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0014_auto_20151005_1124'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='items',
        ),
        migrations.AddField(
            model_name='purchasedstoreitem',
            name='order',
            field=models.ForeignKey(default=1, to='api.Order'),
            preserve_default=False,
        ),
    ]
