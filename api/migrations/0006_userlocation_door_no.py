# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0005_auto_20150907_1828'),
    ]

    operations = [
        migrations.AddField(
            model_name='userlocation',
            name='door_no',
            field=models.CharField(verbose_name='Door No', default='N/A', max_length=250),
            preserve_default=False,
        ),
    ]
