# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0063_order_paid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userbalance',
            name='given_by',
            field=models.ForeignKey(related_name='credited', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
