# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0045_storephoto'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='user_type',
            field=models.CharField(default='customer', max_length=30, verbose_name=b'User Type'),
            preserve_default=False,
        ),
    ]
