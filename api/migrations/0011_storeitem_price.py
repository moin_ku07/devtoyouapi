# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0010_auto_20151001_0443'),
    ]

    operations = [
        migrations.AddField(
            model_name='storeitem',
            name='price',
            field=models.FloatField(default=0.0, verbose_name=b'Float Field'),
            preserve_default=False,
        ),
    ]
