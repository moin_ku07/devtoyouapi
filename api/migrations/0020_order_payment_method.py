# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0019_auto_20151009_0923'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='payment_method',
            field=models.CharField(default='BT', max_length=10, verbose_name=b'Payment Method', choices=[(b'COD', b'Cash On Delivery'), (b'PBC', b'Pay By Credit'), (b'PBP', b'Pay By Points'), (b'BT', b'Bank Transfer'), (b'SPAN', b'Saudi Payment Network')]),
            preserve_default=False,
        ),
    ]
