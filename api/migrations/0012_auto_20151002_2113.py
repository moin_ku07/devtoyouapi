# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0011_storeitem_price'),
    ]

    operations = [
        migrations.AlterField(
            model_name='storeitem',
            name='price',
            field=models.FloatField(verbose_name=b'Price'),
        ),
    ]
