# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0009_auto_20150922_1057'),
    ]

    operations = [
        migrations.AddField(
            model_name='store',
            name='name',
            field=models.CharField(default='N/A', verbose_name='Store Name', max_length=250),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='store',
            name='thumb',
            field=models.ImageField(default='N/A', verbose_name='Thumbnail', upload_to=''),
            preserve_default=False,
        ),
    ]
