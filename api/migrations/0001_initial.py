# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.contrib.auth.models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Bank',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='Bank Name')),
                ('created_at', models.DateTimeField(verbose_name='Created At')),
                ('updated_at', models.DateTimeField(verbose_name='Updated At')),
            ],
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('user_ptr', models.OneToOneField(serialize=False, primary_key=True, to=settings.AUTH_USER_MODEL, parent_link=True, auto_created=True)),
                ('gender', models.CharField(choices=[('M', 'Male'), ('F', 'Female'), ('O', 'Other')], max_length=1, verbose_name='Gender')),
                ('loyalty_amount', models.FloatField(verbose_name='Loyalty Amount')),
                ('deposit_amount', models.FloatField(verbose_name='Deposit Amount')),
                ('primary_phone', models.CharField(max_length=30, verbose_name='Primary Phone')),
                ('created_at', models.DateTimeField(verbose_name='Created At')),
                ('updated_at', models.DateTimeField(verbose_name='Updated At')),
            ],
            options={
                'verbose_name_plural': 'users',
                'abstract': False,
                'verbose_name': 'user',
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Driver',
            fields=[
                ('user_ptr', models.OneToOneField(serialize=False, primary_key=True, to=settings.AUTH_USER_MODEL, parent_link=True, auto_created=True)),
                ('notes', models.TextField()),
                ('created_at', models.DateTimeField(verbose_name='Created At')),
                ('updated_at', models.DateTimeField(verbose_name='Updated At')),
            ],
            options={
                'verbose_name_plural': 'users',
                'abstract': False,
                'verbose_name': 'user',
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('status', models.CharField(choices=[('REQ', 'Request'), ('REV', 'Review'), ('APPR', 'Approved'), ('AD', 'Assigned to Driver'), ('DOTW', 'Driver On The Way'), ('DIS', 'Driver In The Store'), ('DLS', 'Driver Left Store'), ('DELIV', 'Delivered'), ('MCL', 'Moderator Closed'), ('UCAN', 'User Cancelled'), ('MREJ', 'Moderator Rejected Order')], max_length=5, verbose_name='Status')),
                ('pickup', models.DateTimeField(verbose_name='Pick Up Time')),
                ('additional_notes', models.TextField()),
                ('created_at', models.DateTimeField(verbose_name='Created At')),
                ('updated_at', models.DateTimeField(verbose_name='Updated At')),
                ('driver', models.ForeignKey(related_name='driver', to='api.Driver')),
            ],
        ),
        migrations.CreateModel(
            name='OrderEvaluation',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('driver_evalutation', models.TextField(verbose_name='Driver Evaluation')),
                ('driver_rating', models.IntegerField(verbose_name='Driver Rating')),
                ('order_evalutation', models.TextField(verbose_name='Order Evaluation')),
                ('order_rating', models.IntegerField(verbose_name='Order Rating')),
                ('moderator_evalutation', models.TextField(verbose_name='Moderator Evaluation')),
                ('moderator_rating', models.IntegerField(verbose_name='Moderator Rating')),
                ('created_at', models.DateTimeField(verbose_name='Created At')),
                ('updated_at', models.DateTimeField(verbose_name='Updated At')),
                ('order', models.ForeignKey(to='api.Order')),
            ],
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('option', models.CharField(choices=[('B', 'Bank'), ('DC', 'Delivery Credit'), ('COD', 'Cash on Delivery'), ('POS', 'Point of Sale'), ('FD', 'Free Delivery'), ('PA', 'Pay in Advance')], max_length=5, verbose_name='Option')),
                ('bank_user_name', models.CharField(blank=True, null=True, max_length=100, verbose_name='Bank Username')),
                ('transfer_id', models.CharField(max_length=200, verbose_name='Transfer ID')),
                ('transfer_copy', models.ImageField(upload_to='', verbose_name='Copy of Transfer')),
                ('created_at', models.DateTimeField(verbose_name='Created At')),
                ('updated_at', models.DateTimeField(verbose_name='Updated At')),
                ('bank', models.ForeignKey(blank=True, to='api.Bank', null=True)),
                ('order', models.ForeignKey(to='api.Order')),
            ],
        ),
        migrations.CreateModel(
            name='Store',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('description', models.TextField(verbose_name='Description')),
                ('avg_time_spent', models.IntegerField(verbose_name='Average Time Spent (Minutes)')),
                ('store_type', models.CharField(choices=[('R', 'Restaurant'), ('H', 'Home Store'), ('S', 'Sweet Store'), ('O', 'Other')], max_length=3, verbose_name='Sore Type')),
                ('created_at', models.DateTimeField(verbose_name='Created At')),
                ('updated_at', models.DateTimeField(verbose_name='Updated At')),
            ],
        ),
        migrations.CreateModel(
            name='StoreBranch',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=250, verbose_name='Branch Name')),
                ('region', models.CharField(max_length=250, verbose_name='Region')),
                ('phone', models.CharField(max_length=250, verbose_name='Phone')),
                ('longitude', models.CharField(max_length=15, verbose_name='Long')),
                ('latitude', models.CharField(max_length=15, verbose_name='Lat')),
                ('created_at', models.DateTimeField(verbose_name='Created At')),
                ('updated_at', models.DateTimeField(verbose_name='Updated At')),
                ('store', models.ForeignKey(to='api.Store')),
            ],
        ),
        migrations.CreateModel(
            name='StoreEvaluation',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('rating', models.IntegerField(verbose_name='Rating')),
                ('evaluation', models.TextField(verbose_name='Client Feedback')),
                ('created_at', models.DateTimeField(verbose_name='Created At')),
                ('updated_at', models.DateTimeField(verbose_name='Updated At')),
                ('store', models.ForeignKey(to='api.Store')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='StoreItem',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Item name')),
                ('description', models.TextField(verbose_name='Description')),
                ('photo', models.ImageField(upload_to='', verbose_name='Photo')),
                ('avg_rating', models.FloatField(verbose_name='Average Rating')),
                ('total_ratings', models.IntegerField(verbose_name='Total Ratings')),
                ('created_at', models.DateTimeField(verbose_name='Created At')),
                ('updated_at', models.DateTimeField(verbose_name='Updated At')),
                ('store', models.ForeignKey(to='api.Store')),
            ],
        ),
        migrations.CreateModel(
            name='UserLocation',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=250, verbose_name='Location Name')),
                ('region', models.CharField(max_length=250, verbose_name='Region')),
                ('phone', models.CharField(max_length=250, verbose_name='Phone')),
                ('longitude', models.CharField(max_length=15, verbose_name='Long')),
                ('latitude', models.CharField(max_length=15, verbose_name='Lat')),
                ('created_at', models.DateTimeField(verbose_name='Created At')),
                ('updated_at', models.DateTimeField(verbose_name='Updated At')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='order',
            name='store',
            field=models.ForeignKey(to='api.Store'),
        ),
        migrations.AddField(
            model_name='order',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
