# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0030_auto_20151023_0610'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='storeevaluation',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='storeevaluation',
            name='evaluation',
        ),
        migrations.RemoveField(
            model_name='storeevaluation',
            name='updated_at',
        ),
        migrations.RemoveField(
            model_name='storeevaluation',
            name='user',
        ),
        migrations.AddField(
            model_name='storeevaluation',
            name='order',
            field=models.ForeignKey(default=24, to='api.Order'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='storeevaluation',
            name='text',
            field=models.CharField(default='N/A', max_length=250, verbose_name=b'Text'),
            preserve_default=False,
        ),
    ]
