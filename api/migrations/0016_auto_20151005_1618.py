# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0015_auto_20151005_1300'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='total',
            field=models.FloatField(default=0, verbose_name=b'Total'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='purchasedstoreitem',
            name='price',
            field=models.FloatField(default=0, verbose_name=b'Price'),
            preserve_default=False,
        ),
    ]
