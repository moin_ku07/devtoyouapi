# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0049_auto_20151120_1747'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='deposit_amount',
            field=models.FloatField(default=0.0, verbose_name=b'Deposit Amount'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='gender',
            field=models.CharField(default=b'M', max_length=1, verbose_name=b'Gender', choices=[(b'M', b'Male'), (b'F', b'Female'), (b'O', b'Other')]),
        ),
        migrations.AlterField(
            model_name='customer',
            name='loyalty_amount',
            field=models.FloatField(default=0.0, verbose_name=b'Loyalty Amount'),
        ),
    ]
