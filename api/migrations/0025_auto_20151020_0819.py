# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0024_auto_20151018_1009'),
    ]

    operations = [
        migrations.AddField(
            model_name='userlocation',
            name='own_location',
            field=models.BooleanField(default=True, verbose_name=b'Own Location?'),
        ),
        migrations.AlterField(
            model_name='userlocation',
            name='name',
            field=models.CharField(max_length=250, verbose_name=b'Name'),
        ),
    ]
