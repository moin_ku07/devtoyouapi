# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0046_order_user_type'),
    ]

    operations = [
        migrations.CreateModel(
            name='AppSetting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('config', models.CharField(max_length=50, verbose_name=b'Key')),
                ('val', models.CharField(max_length=200, verbose_name=b'Value')),
            ],
        ),
    ]
