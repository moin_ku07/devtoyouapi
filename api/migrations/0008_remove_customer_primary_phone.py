# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0007_socialtoken'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customer',
            name='primary_phone',
        ),
    ]
