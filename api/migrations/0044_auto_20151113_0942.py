# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0043_auto_20151109_2003'),
    ]

    operations = [
        migrations.AddField(
            model_name='userbalance',
            name='store',
            field=models.ForeignKey(blank=True, to='api.Store', null=True),
        ),
        migrations.AlterField(
            model_name='userbalance',
            name='user',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
