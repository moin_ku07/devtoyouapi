# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0023_storeowner'),
    ]

    operations = [
        migrations.AlterField(
            model_name='storeitem',
            name='avg_rating',
            field=models.FloatField(null=True, verbose_name=b'Average Rating', blank=True),
        ),
        migrations.AlterField(
            model_name='storeitem',
            name='total_ratings',
            field=models.IntegerField(null=True, verbose_name=b'Total Ratings', blank=True),
        ),
    ]
