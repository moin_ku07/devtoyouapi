# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0020_order_payment_method'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='user_location',
            field=models.ForeignKey(default=1, to='api.UserLocation'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='order',
            name='additional_notes',
            field=models.TextField(null=True, blank=True),
        ),
    ]
