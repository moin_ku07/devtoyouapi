# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_auto_20150907_1814'),
    ]

    operations = [
        migrations.AddField(
            model_name='userlocation',
            name='door_color',
            field=models.CharField(verbose_name='Door Color', max_length=250, default='N/A'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userlocation',
            name='house_no',
            field=models.CharField(verbose_name='House No', max_length=250, default='N/A'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userlocation',
            name='note',
            field=models.TextField(verbose_name='Note', default='N/A'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userlocation',
            name='street',
            field=models.TextField(verbose_name='Street', default='N/A'),
            preserve_default=False,
        ),
    ]
