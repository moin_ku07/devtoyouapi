# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0057_auto_20160402_0214'),
    ]

    operations = [
        migrations.AlterField(
            model_name='drivergps',
            name='latitude',
            field=models.CharField(max_length=35, verbose_name=b'Lat'),
        ),
        migrations.AlterField(
            model_name='drivergps',
            name='longitude',
            field=models.CharField(max_length=35, verbose_name=b'Long'),
        ),
        migrations.AlterField(
            model_name='storebranch',
            name='latitude',
            field=models.CharField(max_length=35, verbose_name=b'Lat'),
        ),
        migrations.AlterField(
            model_name='storebranch',
            name='longitude',
            field=models.CharField(max_length=35, verbose_name=b'Long'),
        ),
        migrations.AlterField(
            model_name='userlocation',
            name='latitude',
            field=models.CharField(max_length=35, verbose_name=b'Lat'),
        ),
        migrations.AlterField(
            model_name='userlocation',
            name='longitude',
            field=models.CharField(max_length=35, verbose_name=b'Long'),
        ),
    ]
