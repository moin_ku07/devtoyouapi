# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='customer',
            options={'verbose_name': 'Customer', 'verbose_name_plural': 'Customers'},
        ),
        migrations.AlterModelOptions(
            name='driver',
            options={'verbose_name': 'Driver', 'verbose_name_plural': 'Drivers'},
        ),
        migrations.AlterField(
            model_name='storebranch',
            name='store',
            field=models.ForeignKey(to='api.Store', related_name='branches'),
        ),
        migrations.AlterField(
            model_name='storeitem',
            name='store',
            field=models.ForeignKey(to='api.Store', related_name='items'),
        ),
        migrations.AlterField(
            model_name='userlocation',
            name='user',
            field=models.ForeignKey(to='api.Customer', related_name='locations'),
        ),
    ]
