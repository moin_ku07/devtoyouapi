# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0027_drivergps'),
    ]

    operations = [
        migrations.AddField(
            model_name='drivergps',
            name='order',
            field=models.ForeignKey(default=23, to='api.Order'),
            preserve_default=False,
        ),
    ]
