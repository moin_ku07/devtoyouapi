# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0047_appsetting'),
    ]

    operations = [
        migrations.AddField(
            model_name='storephoto',
            name='approved',
            field=models.BooleanField(default=False, verbose_name=b'Is approved?'),
        ),
    ]
