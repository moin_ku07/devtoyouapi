# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20150818_0912'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='photo',
            field=models.ImageField(null=True, blank=True, upload_to='', verbose_name='Photo'),
        ),
        migrations.AddField(
            model_name='driver',
            name='photo',
            field=models.ImageField(null=True, blank=True, upload_to='', verbose_name='Photo'),
        ),
        migrations.AlterField(
            model_name='driver',
            name='created_at',
            field=models.DateTimeField(verbose_name='Created At', auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='driver',
            name='updated_at',
            field=models.DateTimeField(verbose_name='Updated At', auto_now=True),
        ),
    ]
