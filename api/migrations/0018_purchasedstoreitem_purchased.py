# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0017_auto_20151005_1620'),
    ]

    operations = [
        migrations.AddField(
            model_name='purchasedstoreitem',
            name='purchased',
            field=models.BooleanField(default=False, verbose_name=b'Is purchased?'),
        ),
    ]
