# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0044_auto_20151113_0942'),
    ]

    operations = [
        migrations.CreateModel(
            name='StorePhoto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('photo', models.ImageField(upload_to=b'store_photos', verbose_name=b'Store Photo')),
                ('store', models.ForeignKey(to='api.Store')),
            ],
        ),
    ]
