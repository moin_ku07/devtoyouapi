# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0056_driver_gender'),
    ]

    operations = [
        migrations.AddField(
            model_name='store',
            name='delivery_charge',
            field=models.FloatField(default=0.0, verbose_name=b'Delivery Charge'),
        ),
        migrations.AlterField(
            model_name='orderevaluation',
            name='text',
            field=models.CharField(max_length=250, null=True, verbose_name=b'Text', blank=True),
        ),
    ]
