from rest_framework.serializers import ModelSerializer, PrimaryKeyRelatedField, CharField, \
    IntegerField, FloatField
from .models import Driver, Customer, UserLocation, \
    Store, StoreItem, StoreBranch, StoreEvaluation, \
    Bank, Order, Payment, OrderEvaluation, PurchasedStoreItem, \
    OrderStatusChange, StoreOwner, DriverStoreStatus, DriverGPS, \
    ItemEvaluation, DriverEvaluation, UserNotification, CustomerEvaluation, \
    UserBalance, StorePhoto, AppSetting, District, PaymentMethod


class PaymentMethodSerializer(ModelSerializer):
    class Meta:
        model = PaymentMethod


class DistrictSerializer(ModelSerializer):
    class Meta:
        model = District


class AppSettingSerializer(ModelSerializer):
    class Meta:
        model = AppSetting


class StorePhotoSerializer(ModelSerializer):
    class Meta:
        model = StorePhoto


class UserBalanceSerializer(ModelSerializer):
    class Meta:
        model = UserBalance


class UserLocationSerializer(ModelSerializer):
    class Meta:
        model = UserLocation


class CustomerSerializer(ModelSerializer):
    locations = UserLocationSerializer(many=True, read_only=True)
    avg_rating = FloatField(read_only=True)

    class Meta:
        model = Customer
        read_only_fields = ('created_at', 'updated_at',
                            'is_staff', 'is_active',
                            'date_joined', 'groups',
                            'user_permissions', 'is_superuser',
                            'last_login'
                            )

        exclude = ('password', 'is_staff', 'is_active', 'is_superuser', 'user_permissions', 'groups')


class DriverSerializer(ModelSerializer):
    avg_rating = FloatField(read_only=True)

    class Meta:
        model = Driver
        read_only_fields = ('created_at', 'updated_at',
                            'is_staff', 'is_active',
                            'date_joined', 'groups',
                            'user_permissions', 'is_superuser',
                            'last_login'
                            )

        exclude = ('password', 'is_staff', 'is_active', 'is_superuser', 'user_permissions', 'groups')


class StoreItemSerializer(ModelSerializer):
    avg_rating = FloatField(read_only=True)
    store_name = CharField(read_only=True)

    def __init__(self, *args, **kwargs):
        super(StoreItemSerializer, self).__init__(*args, **kwargs)

        context = kwargs.get('context', None)
        if context:
            request = kwargs['context']['request']
            store = request.GET.get('store', False)
            if store:
                self.fields['delivery_charge'] = IntegerField(read_only=True)

    class Meta:
        model = StoreItem


class StoreBranchSerializer(ModelSerializer):
    class Meta:
        model = StoreBranch


class StoreSerializer(ModelSerializer):
    avg_rating = FloatField(read_only=True)

    def __init__(self, *args, **kwargs):
        super(StoreSerializer, self).__init__(*args, **kwargs)

        context = kwargs.get('context', None)
        if context:
            request = kwargs['context']['request']
            include_credit = request.GET.get('include_credit', False)
            if include_credit:
                self.fields['store_balance'] = IntegerField(read_only=True)

    class Meta:
        model = Store


class BankSerializer(ModelSerializer):
    class Meta:
        model = Bank


class PurchasedStoreItemSerializer(ModelSerializer):
    item_name = CharField(read_only=True)
    store_name = CharField(read_only=True)
    store_owner = IntegerField(read_only=True)
    store_id = IntegerField(read_only=True)

    def __init__(self, *args, **kwargs):
        super(PurchasedStoreItemSerializer, self).__init__(*args, **kwargs)

        context = kwargs.get('context', None)
        if context:
            request = kwargs['context']['request']
            include_store_location = request.GET.get('include_store_location', False)
            include_feedback = request.GET.get('include_feedback', False)

            if include_feedback:
                self.fields['items_feedback'] = ItemEvaluationSerializer(read_only=True)

                self.fields['stores_feedback'] = StoreEvaluationSerializer(read_only=True, many=True)

            if include_store_location:
                self.fields['store_branches'] = StoreBranchSerializer(read_only=True, many=True)

    class Meta:
        model = PurchasedStoreItem


class PaymentSerializer(ModelSerializer):
    class Meta:
        model = Payment


class OrderEvaluationSerializer(ModelSerializer):
    class Meta:
        model = OrderEvaluation


class ItemEvaluationSerializer(ModelSerializer):
    class Meta:
        model = ItemEvaluation


class DriverEvaluationSerializer(ModelSerializer):
    given_by = CharField(read_only=True)

    class Meta:
        model = DriverEvaluation


class CustomerEvaluationSerializer(ModelSerializer):
    given_by = CharField(read_only=True)

    class Meta:
        model = CustomerEvaluation


class StoreEvaluationSerializer(ModelSerializer):
    given_by = CharField(read_only=True)

    class Meta:
        model = StoreEvaluation


class DriverStoreStatusSerializer(ModelSerializer):
    class Meta:
        model = DriverStoreStatus


class OrderSerializer(ModelSerializer):
    payments = PaymentSerializer(many=True, read_only=True)
    evaluations = OrderEvaluationSerializer(many=True, read_only=True)
    avg_rating = FloatField(read_only=True)
    display_id = CharField(read_only=True)

    def __init__(self, *args, **kwargs):
        super(OrderSerializer, self).__init__(*args, **kwargs)

        context = kwargs.get('context', None)
        if context:
            request = kwargs['context']['request']
            include_address = request.GET.get('include_address', False)
            dis_dls = request.GET.get('dis_dls', False)
            include_feedback = request.GET.get('include_feedback', False)

            if include_address:
                self.fields['user_location'] = UserLocationSerializer(read_only=True)

            if dis_dls:
                self.fields['dis_dls'] = DriverStoreStatusSerializer(read_only=True, many=True,
                                                                     source='driverstorestatus_set')

            if include_feedback:
                self.fields['order_feedback'] = OrderEvaluationSerializer(read_only=True, many=True,
                                                                          source='orderevaluation_set')

                self.fields['driver_feedback'] = DriverEvaluationSerializer(read_only=True, many=True,
                                                                            source='driverevaluation_set')

                self.fields['customers_feedback'] = CustomerEvaluationSerializer(read_only=True, many=True,
                                                                                 source='customerevaluation_set')

            self.fields['items'] = PurchasedStoreItemSerializer(many=True, read_only=True, source='get_items',
                                                                context=kwargs['context'])

        else:
            self.fields['items'] = PurchasedStoreItemSerializer(many=True, read_only=True, source='get_items')

    class Meta:
        model = Order


class OrderStatusChangeSerializer(ModelSerializer):
    class Meta:
        model = OrderStatusChange


class StoreOwnerSerializer(ModelSerializer):
    class Meta:
        model = StoreOwner


class DriverGPSSerializer(ModelSerializer):
    class Meta:
        model = DriverGPS


class UserNotificationSerializer(ModelSerializer):
    class Meta:
        model = UserNotification
