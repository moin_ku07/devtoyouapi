from django.conf.urls import patterns, url
from djoser import views
from . import base

urlpatterns = base.base_urlpatterns + patterns('',
    url(r'^social/$', views.SocialLoginView.as_view(), name='social_login'),
    url(r'^multi/register$', views.MultiRegisterView.as_view(), name='multi_register'),
    url(r'^multi/login$', views.MultiLogin.as_view(), name='multi_register'),
    url(r'^profile$', views.ProfileView.as_view(), name='profile'),

    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^$', views.RootView.as_view(urls_extra_mapping={'login': 'login', 'logout': 'logout', 'social': 'social_login', 'multi-register': 'multi_register'}), name='root'),
)
