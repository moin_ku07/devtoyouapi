from django.contrib.auth import get_user_model, user_logged_in, user_logged_out, authenticate
from rest_framework import generics, permissions, status, response, views
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.reverse import reverse
from django.contrib.auth.tokens import default_token_generator
from . import serializers, settings, utils, signals
from django.db.models import Q, Sum
from django.conf import settings

from api.models import StoreOwner, Customer, UserBalance

User = get_user_model()


class RootView(views.APIView):
    """
    Root endpoint - use one of sub endpoints.
    """
    permission_classes = (
        permissions.AllowAny,
    )
    urls_mapping = {
        'me': 'user',
        'register': 'register',
        'activate': 'activate',
        'change-' + User.USERNAME_FIELD: 'set_username',
        'change-password': 'set_password',
        'password-reset': 'password_reset',
        'password-reset-confirm': 'password_reset_confirm',
    }
    urls_extra_mapping = None

    def get_urls_mapping(self, **kwargs):
        mapping = self.urls_mapping.copy()
        mapping.update(kwargs)
        if self.urls_extra_mapping:
            mapping.update(self.urls_extra_mapping)
        mapping.update(settings.get('ROOT_VIEW_URLS_MAPPING'))
        return mapping

    def get(self, request, format=None):
        return Response(
            dict([(key, reverse(url_name, request=request, format=format))
                  for key, url_name in self.get_urls_mapping().items()])
        )


class RegistrationView(utils.SendEmailViewMixin, generics.CreateAPIView):
    """
    Use this endpoint to register new user.
    """
    serializer_class = serializers.UserRegistrationSerializer
    permission_classes = (
        permissions.AllowAny,
    )
    token_generator = default_token_generator
    subject_template_name = 'activation_email_subject.txt'
    plain_body_template_name = 'activation_email_body.txt'

    def perform_create(self, serializer):
        instance = serializer.save()
        signals.user_registered.send(sender=self.__class__, user=instance, request=self.request)
        if settings.get('SEND_ACTIVATION_EMAIL'):
            self.send_email(**self.get_send_email_kwargs(instance))

    def get_email_context(self, user):
        context = super(RegistrationView, self).get_email_context(user)
        context['url'] = settings.get('ACTIVATION_URL').format(**context)
        return context


class LoginView(utils.ActionViewMixin, generics.GenericAPIView):
    """
    Use this endpoint to obtain user authentication token.
    """
    serializer_class = serializers.LoginSerializer
    permission_classes = (
        permissions.AllowAny,
    )

    def action(self, serializer):
        user = serializer.user
        try:
            driver = user.driver
        except:
            driver = False

        if driver is not False:
            user_type = "Driver"
        else:
            user_type = "Customer"

        if user_type == "Customer":
            store_owners = StoreOwner.objects.all().filter(customer=user)
            if len(store_owners) > 0:
                user_type = "store_owner"
                store_owner = store_owners[0]
                store_id = store_owner.store.id
            else:
                store_id = None
        else:
            store_id = None

        token, _ = Token.objects.get_or_create(user=user)
        data = {"auth_token": token.key, "user_type": user_type.lower()}

        if store_id:
            data['store_id'] = store_id

        login_serializer = serializers.AuthLoginSerializer(data=data)
        login_serializer.is_valid()

        user_logged_in.send(sender=user.__class__, request=self.request, user=user)

        response_data = {"success": True, "data": login_serializer.data}

        return Response(
            data=response_data,
            status=status.HTTP_200_OK,
        )


class SocialLoginView(utils.ActionViewMixin, generics.GenericAPIView):
    """
    Use this endpoint to obtain user authentication token from social credentials
    """
    serializer_class = serializers.SocialLoginSerializer
    permission_classes = (
        permissions.AllowAny,
    )

    def action(self, serializer):
        return Response(
            data=serializer.data,
            status=status.HTTP_200_OK,
        )


class MultiRegisterView(utils.ActionViewMixin, generics.GenericAPIView):
    """
    Use this endpoint to obtain user authentication token from social credentials
    """
    serializer_class = serializers.MultiRegisterSerializer
    permission_classes = (
        permissions.AllowAny,
    )

    def action(self, serializer):
        return Response(
            data=serializer.data,
            status=status.HTTP_200_OK,
        )


class LogoutView(views.APIView):
    """
    Use this endpoint to logout user (remove user authentication token).
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def post(self, request):
        Token.objects.filter(user=request.user).delete()
        user_logged_out.send(sender=request.user.__class__, request=request, user=request.user)
        return response.Response(status=status.HTTP_200_OK)


class MultiLogin(views.APIView):
    def post(self, request):
        username = request.DATA.get('username', None)
        password = request.DATA.get('password', None)

        users = User.objects.filter(Q(username=username) | Q(email=username))

        if users:
            user = users[0]

            if user.is_active:

                logged_in = authenticate(username=user.username, password=password)
                if logged_in:
                    token, _ = Token.objects.get_or_create(user=user)
                    try:
                        driver = logged_in.driver
                        user_type = 'driver'
                    except:
                        driver = False
                        user_type = 'customer'

                    if user_type == "customer":
                        store_owners = StoreOwner.objects.all().filter(customer=user)
                        if len(store_owners) > 0:
                            user_type = "store_owner"
                            store_owner = store_owners[0]
                            store_id = store_owner.store.id
                        else:
                            store_id = None
                    else:
                        store_id = None

                    data = {
                        'status': True,
                        'auth_token': token.key,
                        'first_name': logged_in.first_name,
                        'last_name': logged_in.last_name,
                        'user_type': user_type,
                        'id': logged_in.id,

                    }

                    if store_id:
                        data['store_id'] = store_id

                    return response.Response(status=status.HTTP_200_OK, data=data)
                else:
                    return response.Response(status=status.HTTP_403_FORBIDDEN, data={"status": False})
            else:
                return response.Response(status=status.HTTP_403_FORBIDDEN, data={"status": False,
                                                                                 "message": "Your account is not active. Contact the admin!"})
        else:
            return response.Response(status=status.HTTP_403_FORBIDDEN, data={"status": False})


class PasswordResetView(utils.ActionViewMixin, utils.SendEmailViewMixin, generics.GenericAPIView):
    """
    Use this endpoint to send email to user with password reset link.
    """
    serializer_class = serializers.PasswordResetSerializer
    permission_classes = (
        permissions.AllowAny,
    )
    token_generator = default_token_generator
    subject_template_name = 'password_reset_email_subject.txt'
    plain_body_template_name = 'password_reset_email_body.txt'

    def action(self, serializer):
        for user in self.get_users(serializer.data['email']):
            self.send_email(**self.get_send_email_kwargs(user))
        return response.Response(status=status.HTTP_200_OK)

    def get_users(self, email):
        active_users = User._default_manager.filter(
            email__iexact=email,
            is_active=True,
        )
        return (u for u in active_users if u.has_usable_password())

    def get_email_context(self, user):
        context = super(PasswordResetView, self).get_email_context(user)
        context['url'] = settings.get('PASSWORD_RESET_CONFIRM_URL').format(**context)
        return context


class SetPasswordView(utils.ActionViewMixin, generics.GenericAPIView):
    """
    Use this endpoint to change user password.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def get_serializer_class(self):
        if settings.get('SET_PASSWORD_RETYPE'):
            return serializers.SetPasswordRetypeSerializer
        return serializers.SetPasswordSerializer

    def action(self, serializer):
        self.request.user.set_password(serializer.data['new_password'])
        self.request.user.save()
        return response.Response(status=status.HTTP_200_OK)


class PasswordResetConfirmView(utils.ActionViewMixin, generics.GenericAPIView):
    """
    Use this endpoint to finish reset password process.
    """
    permission_classes = (
        permissions.AllowAny,
    )
    token_generator = default_token_generator

    def get_serializer_class(self):
        if settings.get('PASSWORD_RESET_CONFIRM_RETYPE'):
            return serializers.PasswordResetConfirmRetypeSerializer
        return serializers.PasswordResetConfirmSerializer

    def action(self, serializer):
        serializer.user.set_password(serializer.data['new_password'])
        serializer.user.save()
        return response.Response(status=status.HTTP_200_OK)


class ActivationView(utils.ActionViewMixin, generics.GenericAPIView):
    """
    Use this endpoint to activate user account.
    """
    serializer_class = serializers.UidAndTokenSerializer
    permission_classes = (
        permissions.AllowAny,
    )
    token_generator = default_token_generator

    def action(self, serializer):
        serializer.user.is_active = True
        serializer.user.save()
        signals.user_activated.send(
            sender=self.__class__, user=serializer.user, request=self.request)
        return Response(status=status.HTTP_200_OK)


class SetUsernameView(utils.ActionViewMixin, generics.GenericAPIView):
    """
    Use this endpoint to change user username.
    """
    serializer_class = serializers.SetUsernameSerializer
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def get_serializer_class(self):
        if settings.get('SET_USERNAME_RETYPE'):
            return serializers.SetUsernameRetypeSerializer
        return serializers.SetUsernameSerializer

    def action(self, serializer):
        setattr(self.request.user, User.USERNAME_FIELD, serializer.data['new_' + User.USERNAME_FIELD])
        self.request.user.save()
        return response.Response(status=status.HTTP_200_OK)


class UserView(generics.RetrieveUpdateAPIView):
    """
    Use this endpoint to retrieve/update user.
    """
    model = User
    serializer_class = serializers.UserSerializer
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def get_object(self, *args, **kwargs):
        return self.request.user


class ProfileView(views.APIView):
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def get(self, request):
        user = request.user
        user_data = {}
        user_data['id'] = user.id
        user_data['username'] = user.username
        user_data['first_name'] = user.first_name
        user_data['last_name'] = user.last_name
        user_data['email'] = user.email

        user_balance = UserBalance.objects.all().filter(user=user).aggregate(balance=Sum('amount'))
        user_data['balance'] = user_balance['balance']

        try:
            driver = user.driver
        except:
            driver = False

        if driver is not False:
            user_type = "driver"
            user_data['type'] = user_type
            user_data['notes'] = driver.notes
            user_data['gender'] = user.driver.gender
            if user.driver.photo:
                user_data['photo'] = request.build_absolute_uri(user.driver.photo.url)

        else:
            user_type = "customer"
            user_data['type'] = user_type
            user_data['gender'] = user.customer.gender
            user_data['loyalty_amount'] = user.customer.loyalty_amount
            user_data['deposit_amount'] = user.customer.deposit_amount
            if user.customer.photo:
                user_data['photo'] = request.build_absolute_uri(user.customer.photo.url)

        return Response(status=status.HTTP_200_OK, data=user_data)
