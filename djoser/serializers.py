from django.contrib.auth import authenticate, get_user_model
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from . import constants, utils
from api.models import SocialToken, Customer, Driver

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = tuple(User.REQUIRED_FIELDS) + (
            User._meta.pk.name,
            User.USERNAME_FIELD,
        )
        read_only_fields = (
            User.USERNAME_FIELD,
        )


class UserRegistrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = tuple(User.REQUIRED_FIELDS) + (
            User.USERNAME_FIELD,
            User._meta.pk.name,
            'password',
        )
        write_only_fields = (
            'password',
        )

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class LoginSerializer(serializers.Serializer):
    password = serializers.CharField(required=False, style={'input_type': 'password'})

    default_error_messages = {
        'inactive_account': constants.INACTIVE_ACCOUNT_ERROR,
        'invalid_credentials': constants.INVALID_CREDENTIALS_ERROR,
    }

    def __init__(self, *args, **kwargs):
        super(LoginSerializer, self).__init__(*args, **kwargs)
        self.user = None
        self.fields[User.USERNAME_FIELD] = serializers.CharField(required=False)

    def validate(self, attrs):
        self.user = authenticate(username=attrs[User.USERNAME_FIELD], password=attrs['password'])
        if self.user:
            if not self.user.is_active:
                raise serializers.ValidationError(self.error_messages['inactive_account'])
            return attrs
        else:
            raise serializers.ValidationError(self.error_messages['invalid_credentials'])


class SocialLoginSerializer(serializers.Serializer):
    service_name = serializers.CharField(required=True)
    profile_id = serializers.CharField(required=True)
    access_token = serializers.CharField(required=True)
    user_type = serializers.CharField(required=False, default='customer')
    first_name = serializers.CharField(required=False, default="N/A")
    last_name = serializers.CharField(required=False, default="N/A")
    email = serializers.CharField(required=False, default="N/A")
    auth_token = serializers.CharField(required=False, read_only=True)
    id = serializers.CharField(required=False)
    # Customer Specific
    gender = serializers.CharField(default='M', required=False)
    loyalty_amount = serializers.FloatField(default=0.00, required=False)
    deposit_amount = serializers.FloatField(default=0.00, required=False)
    primary_phone = serializers.CharField(default='N/A', required=False)

    # Driver Specific
    notes = serializers.CharField(default='N/A', required=False)

    def __init__(self, *args, **kwargs):
        super(SocialLoginSerializer, self).__init__(*args, **kwargs)

    def validate(self, attrs):
        token = None
        id = None
        try:
            social_token = SocialToken.objects.get(service_name=attrs['service_name'], profile_id=attrs['profile_id'])
            token, _ = Token.objects.get_or_create(user=social_token.user)
            id = social_token.user.id
        except Exception as ex:
            if "access_token" in attrs:
                if attrs['user_type'].lower() == 'customer':
                    user = Customer()
                    user.gender = attrs.get('gender', 'M')
                    user.loyalty_amount = attrs.get('loyalty_amount', 0.00)
                    user.deposit_amount = attrs.get('deposit_amount', 0.00)
                    user.primary_phone = attrs.get('primary_phone', 'N/A')

                elif attrs['user_type'].lower() == 'driver':
                    user = Driver()
                    user.notes = attrs.get('notes', 'N/A')
                else:
                    user = User()

                user.username = attrs['service_name'] + "_" + attrs['profile_id']
                user.first_name = attrs['first_name']
                user.last_name = attrs['last_name']
                user.email = attrs['email']
                user.is_active = True
                user.save()

                id = user.id

                social_token = SocialToken()
                social_token.user = user
                social_token.service_name = attrs['service_name']
                social_token.access_token = attrs['access_token']
                social_token.profile_id = attrs['profile_id']

                social_token.save()

                token, _ = Token.objects.get_or_create(user=user)

        attrs['id'] = id
        attrs['auth_token'] = token
        return attrs


class MultiRegisterSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True, write_only=True)

    id = serializers.CharField(read_only=True)

    user_type = serializers.CharField(required=False, default='customer')
    first_name = serializers.CharField(required=False, default="N/A")
    last_name = serializers.CharField(required=False, default="N/A")
    email = serializers.CharField(required=False, default="N/A")
    auth_token = serializers.CharField(required=False, read_only=True)

    # Customer Specific
    gender = serializers.CharField(default='M', required=False)
    loyalty_amount = serializers.FloatField(default=0.00, required=False)
    deposit_amount = serializers.FloatField(default=0.00, required=False)

    # Driver Specific
    notes = serializers.CharField(default='N/A', required=False)

    def __init__(self, *args, **kwargs):
        super(MultiRegisterSerializer, self).__init__(*args, **kwargs)

    def validate(self, attrs):
        token = None
        users = User.objects.all().filter(username=attrs['username'])
        if not users:
            if attrs['user_type'].lower() == 'customer':
                user = Customer()
                user.gender = attrs.get('gender', 'M')
                user.loyalty_amount = attrs.get('loyalty_amount', 0.00)
                user.deposit_amount = attrs.get('deposit_amount', 0.00)
                user.primary_phone = attrs.get('primary_phone', 'N/A')

            elif attrs['user_type'].lower() == 'driver':
                user = Driver()
                user.notes = attrs.get('notes', 'N/A')
            else:
                user = User()

            user.username = attrs[User.USERNAME_FIELD]
            user.first_name = attrs['first_name']
            user.last_name = attrs['last_name']
            user.email = attrs['email']
            user.is_active = True

            user.set_password(attrs['password'])

            user.save()

            token, _ = Token.objects.get_or_create(user=user)

        else:
            raise serializers.ValidationError("User already exists!")

        attrs['id'] = user.id
        attrs['auth_token'] = token
        return attrs


class PasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField()


class UidAndTokenSerializer(serializers.Serializer):
    uid = serializers.CharField()
    token = serializers.CharField()

    default_error_messages = {
        'invalid_token': constants.INVALID_TOKEN_ERROR
    }

    def validate_uid(self, value):
        try:
            uid = utils.decode_uid(value)
            self.user = User.objects.get(pk=uid)
        except (User.DoesNotExist, ValueError, TypeError, ValueError, OverflowError) as error:
            raise serializers.ValidationError(error)
        return value

    def validate(self, attrs):
        attrs = super(UidAndTokenSerializer, self).validate(attrs)
        if not self.context['view'].token_generator.check_token(self.user, attrs['token']):
            raise serializers.ValidationError(self.error_messages['invalid_token'])
        return attrs


class PasswordSerializer(serializers.Serializer):
    new_password = serializers.CharField(style={'input_type': 'password'})


class PasswordRetypeSerializer(PasswordSerializer):
    re_new_password = serializers.CharField(style={'input_type': 'password'})

    default_error_messages = {
        'password_mismatch': constants.PASSWORD_MISMATCH_ERROR,
    }

    def validate(self, attrs):
        attrs = super(PasswordRetypeSerializer, self).validate(attrs)
        if attrs['new_password'] != attrs['re_new_password']:
            raise serializers.ValidationError(self.error_messages['password_mismatch'])
        return attrs


class CurrentPasswordSerializer(serializers.Serializer):
    current_password = serializers.CharField(style={'input_type': 'password'})

    default_error_messages = {
        'invalid_password': constants.INVALID_PASSWORD_ERROR,
    }

    def validate_current_password(self, value):
        if not self.context['request'].user.check_password(value):
            raise serializers.ValidationError(self.error_messages['invalid_password'])
        return value


class SetPasswordSerializer(PasswordSerializer, CurrentPasswordSerializer):
    pass


class SetPasswordRetypeSerializer(PasswordRetypeSerializer, CurrentPasswordSerializer):
    pass


class PasswordResetConfirmSerializer(UidAndTokenSerializer, PasswordSerializer):
    pass


class PasswordResetConfirmRetypeSerializer(UidAndTokenSerializer, PasswordRetypeSerializer):
    pass


class SetUsernameSerializer(serializers.ModelSerializer, CurrentPasswordSerializer):
    class Meta(object):
        model = User
        fields = (
            User.USERNAME_FIELD,
            'current_password',
        )

    def __init__(self, *args, **kwargs):
        super(SetUsernameSerializer, self).__init__(*args, **kwargs)
        self.fields['new_' + User.USERNAME_FIELD] = self.fields[User.USERNAME_FIELD]
        del self.fields[User.USERNAME_FIELD]


class SetUsernameRetypeSerializer(SetUsernameSerializer):
    default_error_messages = {
        'username_mismatch': constants.USERNAME_MISMATCH_ERROR.format(User.USERNAME_FIELD),
    }

    def __init__(self, *args, **kwargs):
        super(SetUsernameRetypeSerializer, self).__init__(*args, **kwargs)
        self.fields['re_new_' + User.USERNAME_FIELD] = serializers.CharField()

    def validate(self, attrs):
        attrs = super(SetUsernameRetypeSerializer, self).validate(attrs)
        new_username = attrs[User.USERNAME_FIELD]
        if new_username != attrs['re_new_' + User.USERNAME_FIELD]:
            raise serializers.ValidationError(self.error_messages['username_mismatch'].format(User.USERNAME_FIELD))
        return attrs


class TokenSerializer(serializers.ModelSerializer):
    auth_token = serializers.CharField(source='key')

    class Meta:
        model = Token
        fields = (
            'auth_token',
        )


class AuthLoginSerializer(serializers.Serializer):
    auth_token = serializers.CharField(required=True, allow_blank=False, max_length=100)
    user_type = serializers.CharField(required=True, allow_blank=False, max_length=15)
    store_id = serializers.IntegerField(required=False)
