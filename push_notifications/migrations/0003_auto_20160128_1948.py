# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('push_notifications', '0002_auto_20160106_0850'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gcmdevice',
            name='registration_id',
            field=models.CharField(unique=True, max_length=255, verbose_name='Registration ID'),
        ),
    ]
